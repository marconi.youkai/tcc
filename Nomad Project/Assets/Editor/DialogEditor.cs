﻿using UnityEditor;

// [CustomEditor(typeof(Dialog))]
public class DialogEditor : Editor
{
    private string[] characters;
    private int characterIndex = 0;
    private DialogSystem.Dialog dialog;

    private void OnEnable()
    {
        dialog = target as DialogSystem.Dialog;
        characters = new string[0];
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        // base.DrawDefaultInspector();

        SerializedObject so = new SerializedObject(target);
        SerializedProperty dialogList = so.FindProperty("dialogBoxes");
        SerializedProperty characterArray = so.FindProperty("_characters");

        EditorGUILayout.PropertyField(characterArray);

        EditorGUILayout.PropertyField(dialogList);
        
        if (characters.Length != dialog.Characters.Length)
        {
            EditorGUI.BeginChangeCheck();
            characters = new string[dialog.Characters.Length];
            for (int i = 0; i < dialog.Characters.Length; i++)
            {
                characters[i] = dialog.Characters[i].name;
            }

            EditorGUI.EndChangeCheck();
        }

        for (int i = 0; i < dialog.DialogBoxes.Length; i++)
        {
            SerializedProperty SPArray = dialogList.GetArrayElementAtIndex(i).FindPropertyRelative("character");

            for (int j = 0; j < dialog.Characters.Length; j++)
            {
                if (dialog.DialogBoxes[i].character.name == dialog.Characters[j].name)
                {
                    characterIndex = j;
                    break;
                }
            }


            EditorGUI.BeginChangeCheck();


            characterIndex = EditorGUILayout.Popup(characterIndex, characters);

            dialog.DialogBoxes[i].character = dialog.Characters[characterIndex];
            EditorGUILayout.PropertyField(SPArray);

            if (EditorGUI.EndChangeCheck())
            {
                so.ApplyModifiedProperties();
            }
        }
    }
}