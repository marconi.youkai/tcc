/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_CAVE_AMB = 1444571876U;
        static const AkUniqueID PLAY_CAVE_LEVEL = 2423530510U;
        static const AkUniqueID PLAY_JAR_CLINK = 3710709927U;
        static const AkUniqueID PLAY_KITCHEN_DOOR_CLOSE = 199194952U;
        static const AkUniqueID PLAY_KITCHEN_DOOR_OPEN = 2165807276U;
        static const AkUniqueID PLAY_LIGHT_STEP = 3234743103U;
        static const AkUniqueID PLAY_LIGHTSWITCH = 979756790U;
        static const AkUniqueID PLAY_MENU_MOVE = 1871533573U;
        static const AkUniqueID PLAY_MENU_SELECT = 2167056030U;
        static const AkUniqueID PLAY_NERFAMMO_PICKUP = 24406978U;
        static const AkUniqueID PLAY_NERFGUN_EMPTY = 863908115U;
        static const AkUniqueID PLAY_NERFGUN_HIT = 3091663903U;
        static const AkUniqueID PLAY_NERFGUN_SHOT = 2552912056U;
        static const AkUniqueID PLAY_TEST1 = 3759155919U;
        static const AkUniqueID ROBOT_CHASE = 3129072800U;
        static const AkUniqueID ROBOT_CHASE_END = 133209574U;
        static const AkUniqueID STOP_CHASELOOP = 1433577460U;
        static const AkUniqueID STOP_TEST1 = 2011175805U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace THREAT
        {
            static const AkUniqueID GROUP = 1959898597U;

            namespace STATE
            {
                static const AkUniqueID DANGER = 4174463524U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID SAFE = 938058686U;
            } // namespace STATE
        } // namespace THREAT

    } // namespace STATES

    namespace SWITCHES
    {
        namespace FLOOR_MATERIAL
        {
            static const AkUniqueID GROUP = 3964766495U;

            namespace SWITCH
            {
                static const AkUniqueID CAVE = 4122393694U;
                static const AkUniqueID CONCRETE = 841620460U;
                static const AkUniqueID SAND = 803837735U;
            } // namespace SWITCH
        } // namespace FLOOR_MATERIAL

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MOVEMENTNOISEAMOUNT = 770984900U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID ENEMYVISION = 1406033083U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID CHAR = 4140304029U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID REVERBS = 3545700988U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID CAVE_VERB = 3811431050U;
        static const AkUniqueID ROOM_VERB = 821123712U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
