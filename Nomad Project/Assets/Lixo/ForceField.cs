﻿using UnityEngine;

public class ForceField : MonoBehaviour
{
    [SerializeField] private Material ff;
    private float distortAmount = 0;
    [SerializeField] private float release = 0.1f;
    [SerializeField] private float idleDistort = 0f;
    [SerializeField] private ForceField[] childFFields;

    private float multiplier = 0;

    private AudioSource audioS;
    [SerializeField] private AudioClip bounce;

    private void Start()
    {
        distortAmount = idleDistort;
        audioS = GetComponent<AudioSource>();
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.layer == 8)
    //    {
    //        print("entrou");
    //        distortAmount = 0.08f;
    //        //Destroy(other.gameObject);
    //    }
    //}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce((collision.collider.ClosestPoint(transform.position) - transform.position) * 2 * (1 + multiplier), ForceMode.Impulse);
            TakeHit();
            foreach(ForceField ffield in childFFields)
            {
                ffield.TakeHit();
            }
        }
    }

    public void TakeHit()
    {
        distortAmount = idleDistort + (0.08f * (1 + multiplier));
        multiplier += 0.05f * (1 + multiplier);
        float audioMult = 1 + (multiplier * 0.09f);
        audioS.pitch = Random.Range(0.8f * audioMult, 0.95f * audioMult);
        audioS.PlayOneShot(bounce);
    }

    private void Update()
    {
        float deltaTime = Time.deltaTime;

        if (distortAmount > idleDistort)
        {
            distortAmount = distortAmount - deltaTime * release * (1 + multiplier);
            if (distortAmount < idleDistort)
                distortAmount = idleDistort;
        }
        ff.SetFloat("Vector1_98AA3D82", distortAmount);

        if (multiplier > 0)
        {
            multiplier -= deltaTime * release * 5f * (1 + multiplier);
        }
    }
}
