﻿using UnityEngine;

public class arma : MonoBehaviour
{
    [SerializeField] private GameObject tiro;
    [SerializeField] private Transform spawn;
    [SerializeField] private float forcaDoTiro = 2f;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject temp = Instantiate(tiro, spawn.position, spawn.rotation);
            temp.GetComponent<Rigidbody>().AddForce(temp.transform.forward * forcaDoTiro * 10 * Random.Range(0.9f, 1.1f), ForceMode.Impulse);
        }
    }
}
