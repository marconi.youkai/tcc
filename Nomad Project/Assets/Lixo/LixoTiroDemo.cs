﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LixoTiroDemo : MonoBehaviour
{
    public Transform wp1;
    public Transform wp2;
    public Light luz;
    private float initialLight;
    // Start is called before the first frame update
    void Start()
    {
        initialLight = luz.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        transform.position = Vector3.MoveTowards(transform.position, wp1.position, 30 * Time.deltaTime);
        if (transform.position == wp1.position)
        {
            transform.position = wp2.position;
        }
        if (luz.intensity > initialLight)
            luz.intensity = luz.intensity - 50 * Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        luz.intensity = 6f;
    }
}
