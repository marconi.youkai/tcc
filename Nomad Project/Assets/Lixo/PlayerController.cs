﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{

    public Camera cam;

    public NavMeshAgent agent;
    public NavMeshAgent flyingAgent;

    public float walkSpeed = 6.0f;
    public float runSpeed = 10.0f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //verificar um timer para ver se existe clique duplo para alterar a velocidade entre walkSpeed e runSpeed ao inves de usar o shift
            //define a velocidade do player
            agent.speed = walkSpeed;
            if (Input.GetKey(KeyCode.LeftShift))
            {
                agent.speed = runSpeed;
            }

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                //Move agent
                agent.SetDestination(hit.point);
            }
        }
    }
}
