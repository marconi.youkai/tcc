﻿using UnityEngine;

[ExecuteInEditMode]
public class GetLight : MonoBehaviour
{
    
    public Material material;
    public Transform player;
    public Light light;

    Vector3 posAbs;

    

    // Start is called before the first frame update
    void Start()
    {
        posAbs = transform.position;
        
       // StartCoroutine("writeToMaterial");
    }

    /*IEnumerator writeToMaterial()
    {
        while(true)
        {
            material.SetVector("_lightDir", light.transform.forward.normalized);
            yield return null;
        }
    }*/
    void OnValidate() {
            
            Update();
        }
    // Update is called once per frame
    void Update()
    {
        //Debug.Log( light.transform.forward);
        UpdateMaterial();
    }

    private void UpdateMaterial()
    {
        /*
        Vector3 dir = light.transform.position - posAbs;
        material.SetVector("_lightDir", dir);
        float dist = Mathf.Clamp01(dir.magnitude / light.range);
        float atten = CalcAttenuation(dist);
        Color color = light.color * atten * light.intensity * 0.1f;
        color.a = Mathf.Clamp(atten, 0.01f, 0.99f);
        material.SetColor("_lightColor", color);*/
        Vector3 dir = light.transform.forward * -1f;
        material.SetVector("_lightDir", dir);
        Color color = light.color * light.intensity;
        color.a = Mathf.Clamp(1f, 0.01f, 0.99f);
        material.SetColor("_lightColor", color);
    }
    float CalcAttenuation(float dist) {
            return Mathf.Clamp01(1.0f / (1.0f + 25f * dist * dist) * Mathf.Clamp01((1f - dist) * 5f));
        }
}
