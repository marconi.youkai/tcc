public interface ISensor
{
    bool Detected(EnemyCharacter enemy, PlayerCharacter character);
}