﻿using UnityEngine;

public class AudioEvent : MonoBehaviour
{
    [HideInInspector] public PlayerCharacter player;
    public string eventName = "Play_Light_Step";
    private void Start()
    {
        //AkSoundEngine.RegisterGameObj(gameObject);
    }
    public void Step()
    {
        player.PlaySound(eventName);
    }
}
