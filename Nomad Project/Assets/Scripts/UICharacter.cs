﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Dialog system/New Character", order = 0)]
public class UICharacter : ScriptableObject
{
    [SerializeField] private string characterName;
    [SerializeField] private Sprite characterImg;
    public string Name => characterName;
    public Sprite Image => characterImg;
}