﻿using UnityEngine;
using UnityEngine.Events;

public class ClickableTrigger : MonoBehaviour, IInteractable<ICharacter>, IMouseOverable
{
    [SerializeField] private UnityEvent _triggeredEvents;

    public bool Interact(ICharacter condition)
    {
        _triggeredEvents.Invoke();
#if UNITY_EDITOR
        Debug.Log("event triggered", this);
#endif
        return true;
    }

    public CursorManager.CursorType MouseOverCursorType()
    {
        MouseOverRaycaster.Instance.StartContinuousRefresh();
        if (Vector3.Distance(InventoryManager.GetInventoryManager().activePlayer.transform.position, transform.position) <= 1.4f)
            return CursorManager.CursorType.Rightclick;
        else
            return CursorManager.CursorType.Unavailable;
    }

    public CursorManager.CMStates[] MouseOverStatesAllowed()
    {
        return new CursorManager.CMStates[1] { CursorManager.CMStates.InGame };
    }

    public string MouseOverTooltip()
    {
        return "Right-Click to interact.\nYou need to be close!";
    }
}
