﻿using UnityEngine;

public class Safezone : MonoBehaviour
{
    public bool isWinCondition = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PlayerCharacter filha))
        {
            filha.insideSafeZone = true;
            if (isWinCondition)
                filha.Win();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PlayerCharacter filha))
        {
            filha.insideSafeZone = false;
        }
    }
}
