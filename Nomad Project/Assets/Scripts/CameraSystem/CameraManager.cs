using System;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;

// using GD.MinMaxSlider;

namespace CameraSystem
{
    public class CameraManager : MonoBehaviour
    {
        private static CameraManager _cameraManager;
        public static CameraManager instance => _cameraManager;

        public delegate void ZoomChange();

        public static event ZoomChange OnZoomChange;


        [Header("Camera prefabs")]
        [SerializeField] private GameObject _freeCameraPrefab;

        [SerializeField] private GameObject _followCameraPrefab;

        [Header("Camera settings")]
        [SerializeField] /*, MinMaxSlider(5, 35)]*/private Vector2 CameraDistanceRange = new Vector2(10, 35);

        private Dictionary<GameObject, CinemachineVirtualCamera> _cameraDictionary = new Dictionary<GameObject, CinemachineVirtualCamera>();
        private CinemachineVirtualCamera _followCamera;
        private CinemachineVirtualCamera _freeCamera;
        private GameObject SelectedPlayer = null;
        private GameObject _dummyTarget;
        public CinemachineVirtualCamera freeCamera => _freeCamera;

        public GameObject selectedPlayer => SelectedPlayer;

        public GameObject dummyTarget => _dummyTarget;


        #region CameraSettings

        [SerializeField, Range(1f, 100f), Tooltip("Da distância mínima pra máxima da câmera, quantos passos?")]
        private int scrollSteps = 10;

        [SerializeField, Range(1f, 100f), Tooltip("Starting distance from the cameras to the target")]
        private float _camDistance = 23f;

        public float CameraDistance => _camDistance;

        #endregion

        private CameraManager()
        {
        }

        private void Awake()
        {
            _cameraManager = this;

            LoadCamPrefabs();

            FindPlayerCameras();

            var foundCam = GameObject.FindGameObjectWithTag(Constants.FreeCameraTag);

            if (!foundCam)
            {
                _freeCamera = GameObject.Instantiate(_freeCameraPrefab, _freeCameraPrefab.transform.position, _freeCameraPrefab.transform.rotation).GetComponent<CinemachineVirtualCamera>();
            }
            else
            {
                _freeCamera = foundCam.GetComponent<CinemachineVirtualCamera>();
            }

            if (!_freeCamera.Follow)
            {
                _dummyTarget = new GameObject("Free camera dummy target");
                _dummyTarget.transform.rotation = Quaternion.AngleAxis(_freeCamera.transform.rotation.eulerAngles.y, Vector3.up);
                _dummyTarget.AddComponent<AkAudioListener>();
                AkSoundEngine.RegisterGameObj(_dummyTarget);
                _freeCamera.m_Follow = dummyTarget.transform;
            }

            _freeCamera.Priority = 0;

            this.SelectedPlayer = _cameraDictionary.ElementAt(0).Key;
        }

        private void Start()
        {
            dummyTarget.transform.position = _cameraDictionary.ElementAt(0).Key.transform.position;
        }

        private void OnValidate()
        {
            if (_camDistance > CameraDistanceRange.x && _camDistance < CameraDistanceRange.y)
                return;
            else
            {
                if (_camDistance < CameraDistanceRange.x)
                    _camDistance = CameraDistanceRange.x;
                if (_camDistance > CameraDistanceRange.y)
                    _camDistance = CameraDistanceRange.y;
            }
        }

        private void LoadCamPrefabs()
        {
            if (!_freeCameraPrefab ||
                !_freeCameraPrefab.GetComponent<CinemachineVirtualCamera>() ||
                !_freeCameraPrefab.gameObject.CompareTag(Constants.FreeCameraTag))
            {
                _freeCameraPrefab = Resources.Load<GameObject>("CM cams/VC Free view");
                Debug.LogWarning("Free camera prefab loaded!");
            }


            if (!_followCameraPrefab ||
                !_followCameraPrefab.GetComponent<CinemachineVirtualCamera>() ||
                !_followCameraPrefab.gameObject.CompareTag(Constants.PlayerCameraTag))
            {
                _followCameraPrefab = Resources.Load<GameObject>("CM cams/VC Player locked");
                Debug.LogWarning("Follow camera prefab loaded!");
            }
        }

        private void AddCamerasToDictionary(Tuple<GameObject, CinemachineVirtualCamera> camTuple)
        {
            var player = camTuple.Item1;
            var cam = camTuple.Item2;

            if (_cameraDictionary.ContainsKey(player))
            {
                Debug.LogError("Player " + player.name + " already have a camera deleting camera :" + cam.gameObject, cam.gameObject);
                GameObject.Destroy(cam.gameObject);
                return;
            }

            _cameraDictionary.Add(player, cam);

            if (_cameraDictionary.Count > 1)
            {
                _cameraDictionary[player].Priority = 0;
            }
        }

        private void FindPlayerCameras()
        {
            var camList = GameObject.FindGameObjectsWithTag(Constants.PlayerCameraTag);

            List<GameObject> playersInScene = GameObject.FindGameObjectsWithTag(Constants.PlayerTag).ToList();

            List<CinemachineVirtualCamera> strayCameras = new List<CinemachineVirtualCamera>();

            foreach (var cam in camList)
            {
                var currentCam = cam.GetComponent<CinemachineVirtualCamera>();
                if (currentCam.Follow == null)
                {
                    strayCameras.Add(currentCam);
                    continue;
                }

                GameObject cineFollow = currentCam.Follow.gameObject;

                if (cineFollow.CompareTag(Constants.PlayerTag))
                {
                    AddCamerasToDictionary(new Tuple<GameObject, CinemachineVirtualCamera>(cineFollow, currentCam));
                    playersInScene.Remove(cineFollow);
                }
            }

            for (int i = 0; i < playersInScene.Count; i++)
            {
                for (int j = 0; j < strayCameras.Count; j++)
                {
                    strayCameras[j].Follow = playersInScene[i].transform;
                    AddCamerasToDictionary(new Tuple<GameObject, CinemachineVirtualCamera>(playersInScene[i], strayCameras[j]));
                    Debug.LogWarning("Linked player " + playersInScene[i].name + " to Camera " + strayCameras[j].gameObject.name, strayCameras[j].gameObject);
                    return;
                }

                AddCamerasToDictionary(CreatePlayerCamera(playersInScene[i].gameObject));
            }

            // return null;
        }


        private Tuple<GameObject, CinemachineVirtualCamera> CreatePlayerCamera(GameObject player)
        {
            if (!player.CompareTag(Constants.PlayerTag))
                return null;

            if (_cameraDictionary.ContainsKey(player))
                return null;

            var newCam = GameObject.Instantiate(_followCameraPrefab, _followCameraPrefab.transform.position, _followCameraPrefab.transform.rotation).GetComponent<CinemachineVirtualCamera>();
            newCam.Follow = player.transform;

            Debug.LogWarning("Camera Created for player " + player, newCam.gameObject);
            return new Tuple<GameObject, CinemachineVirtualCamera>(player, newCam);
        }

        public void ChangeCharCam(GameObject player)
        {
            if (!_cameraDictionary.ContainsKey(player))
            {
#if UNITY_EDITOR
                Debug.LogError("Player " + player.gameObject.name + "not found in active camera dictionaries", player);
#endif
                return;
            }

            _cameraDictionary[player].Priority = 8;
            _cameraDictionary[SelectedPlayer].Priority = 0;
            ToggleFreeCamera();
            this.SelectedPlayer = player;
        }

        public void CameraZoom(int step)
        {
            var increment = ((CameraDistanceRange.y - CameraDistanceRange.x) / scrollSteps) * step;

            this._camDistance = Mathf.Clamp(_camDistance - increment, CameraDistanceRange.x, CameraDistanceRange.y);

            if (OnZoomChange != null) OnZoomChange();
        }

        public void ToggleFreeCamera()
        {
            if (this._freeCamera.Priority != 0)
            {
                this._freeCamera.Priority = 0;
                this._dummyTarget.transform.position = _cameraDictionary[this.SelectedPlayer].Follow.transform.position;
            }
            else
            {
                this._dummyTarget.transform.position = _cameraDictionary[this.SelectedPlayer].Follow.transform.position;
            }
        }
    }
}