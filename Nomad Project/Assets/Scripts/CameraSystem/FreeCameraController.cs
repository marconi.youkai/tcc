using System;
using Unity.Collections;
using UnityEngine;


namespace CameraSystem
{
    public class FreeCameraController : MonoBehaviour
    {
        private CameraManager _cameraManager;
        private Transform dummyTarget;

        [SerializeField, Range(0.0f, 100.0f)] private float camSpeed = 0.5f;
        [SerializeField] private bool useCurveForCameraSpeed = false;
        [SerializeField] private AnimationCurve camSpeedCurve = new AnimationCurve(new Keyframe(0.0f,0.0f), new Keyframe(1.0f,10.0f));

        /// <summary>
        /// In percentage relative to screen height
        /// </summary>
        [SerializeField, Range(0.0f, 100.0f), Tooltip("Zone determined in percent relative to screen height")] private float detectionZone = 5.0f;

        private int DetectionZonePixel => (int) (screenHeight * (Remap(detectionZone, 0.0f, 100.0f) / 2));
        private float DetectionZone => Remap(detectionZone, 0.0f, 100.0f); // divided by 100 to give a value from 0.0f ~ 1.0f

        private Vector3 mousePos = Vector3.zero;
        private int screenWidht = 0, screenHeight = 0;

        private void Start()
        {
            _cameraManager = CameraManager.instance;
            dummyTarget = _cameraManager.dummyTarget.transform;
            screenWidht = Screen.width;
            screenHeight = Screen.height;
        }

        private void FixedUpdate()
        {
            Move();
        }

        private void LateUpdate()
        {
            dummyTarget.transform.rotation = Quaternion.Euler(0, -45, 0);
        }

        private void Move()
        {
            mousePos = Input.mousePosition;


            if (_cameraManager.freeCamera.Priority == 0)
            {
                Transform dummyTargetTransform;
                (dummyTargetTransform = dummyTarget.transform).SetParent(_cameraManager.selectedPlayer.transform);
                dummyTargetTransform.localPosition = Vector3.zero;
            }
            else
            {
                dummyTarget.SetParent(null);
            }

            //check for left detection area
            if (mousePos.x <= DetectionZonePixel)
            {
                _cameraManager.freeCamera.Priority = 10;
                var speedMultiplier = Remap(mousePos.x, DetectionZonePixel, 0.0f);
                dummyTarget.Translate(-dummyTarget.right * ((camSpeed * (useCurveForCameraSpeed ? camSpeedCurve.Evaluate(speedMultiplier) : speedMultiplier)) * Time.fixedDeltaTime), Space.World);
            }

            //check for right detection area
            if (mousePos.x >= screenWidht - DetectionZonePixel)
            {
                _cameraManager.freeCamera.Priority = 10;
                var speedMultiplier = Remap(mousePos.x, screenWidht - DetectionZonePixel, screenWidht);
                dummyTarget.Translate(dummyTarget.right * ((camSpeed * (useCurveForCameraSpeed ? camSpeedCurve.Evaluate(speedMultiplier) : speedMultiplier)) * Time.fixedDeltaTime), Space.World);
            }

            //check for upper detection area
            if (mousePos.y <= DetectionZonePixel)
            {
                _cameraManager.freeCamera.Priority = 10;
                var speedMultiplier = Remap(mousePos.y, DetectionZonePixel, 0.0f);
                dummyTarget.Translate(-dummyTarget.forward * ((camSpeed * (useCurveForCameraSpeed ? camSpeedCurve.Evaluate(speedMultiplier) : speedMultiplier)) * Time.fixedDeltaTime), Space.World);
            }

            //check for bottom detection area
            if (mousePos.y >= screenHeight - DetectionZonePixel)
            {
                _cameraManager.freeCamera.Priority = 10;
                var speedMultiplier = Remap(mousePos.y, screenHeight - DetectionZonePixel, screenHeight);
                dummyTarget.Translate(dummyTarget.forward * ((camSpeed * (useCurveForCameraSpeed ? camSpeedCurve.Evaluate(speedMultiplier) : speedMultiplier)) * Time.fixedDeltaTime), Space.World);
            }
        }
        
        /// <summary>
        /// Remap from value from specified interval to desired interval ( if no desired interval is specified will remap to 0.0f up to 1.0f)
        /// </summary>
        /// <param name="value">Value to be remapped</param>
        /// <param name="originalLow">Lowest sampled value</param>
        /// <param name="originalHigh">Highest sampled value</param>
        /// <param name="outputLow">Lowest remapped value</param>
        /// <param name="outputHigh">Highest remapped value</param>
        /// <returns>Remapped value between output low and output high</returns>
        private float Remap(float value, float originalLow, float originalHigh, float outputLow = 0.0f, float outputHigh = 1.0f)
        {
            return (value - originalLow) / (originalHigh - originalLow) * (outputHigh - outputLow) + outputLow;
        }
    }
}