using Cinemachine;
using UnityEngine;


namespace CameraSystem
{
    public class ZoomChanger : MonoBehaviour
    {
        private CinemachineFramingTransposer _camTransposer;

        private void Start()
        {
            this._camTransposer = this.gameObject.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineFramingTransposer>();
            this._camTransposer.m_CameraDistance = CameraManager.instance.CameraDistance;
        }

        private void OnEnable()
        {
            CameraManager.OnZoomChange += UpdateZoom;
        }

        private void OnDisable()
        {
            CameraManager.OnZoomChange -= UpdateZoom;
        }

        private void UpdateZoom()
        {
            this._camTransposer.m_CameraDistance = CameraManager.instance.CameraDistance;
        }
    }
}