﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI versionText;

    private void Start()
    {
        versionText.text = Application.version;
    }

    public void StartGame()
    {
        SceneManager.LoadScene("CinematicIntro");
    }

    public void ToCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void Apresentacao()
    {
        SceneManager.LoadScene("Apresentacao");
    }

    public void Teaser()
    {
        SceneManager.LoadScene("Teaser");
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
}