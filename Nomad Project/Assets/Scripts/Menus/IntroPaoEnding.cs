﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroPaoEnding : MonoBehaviour
{
    [SerializeField] VideoPlayer vp;
    [SerializeField] GameObject aguardando;
    [SerializeField] Image fadeOut;
    bool started = false;
    [SerializeField] bool buildForStreaming = false;

    private void Start()
    {
        if (!buildForStreaming)
        {
            started = true;
            aguardando.SetActive(false);
            vp.Prepare();
            vp.Play();
            return;
        }
    }

    private void Update()
    {
        if (!started && Input.GetKeyDown(KeyCode.Return))
            StartCoroutine(PlayPao());
        if ((int)vp.frame == (int)vp.frameCount - 1)
            SceneManager.LoadScene("MainMenu");
    }

    private IEnumerator PlayPao()
    {
        started = true;
        float alpha = 0;
        while (alpha < 1)
        {
            alpha += 0.05f;
            fadeOut.color = new Color(fadeOut.color.r, fadeOut.color.g, fadeOut.color.b, alpha);
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(0.3f);
        aguardando.SetActive(false);
        vp.Prepare();
        vp.Play();
    }
}
