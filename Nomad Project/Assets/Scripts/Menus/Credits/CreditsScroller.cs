﻿using UnityEngine;

public class CreditsScroller : MonoBehaviour
{

    private Transform creditsTrans;
    [SerializeField] private float CreditsSpeed = 5.0f;
    private void Start()
    {
        creditsTrans = this.transform;
    }

    private void FixedUpdate()
    {
        creditsTrans.Translate(Vector3.up * (CreditsSpeed * Time.fixedDeltaTime) * (Screen.width / 1920f));
    }
}
