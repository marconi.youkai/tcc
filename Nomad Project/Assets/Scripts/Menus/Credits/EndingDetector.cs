﻿using UnityEngine;

public class EndingDetector : MonoBehaviour
{
    private ReturnToMainMenu _returnToMainMenu;
    private void Start()
    {
        this._returnToMainMenu = GameObject.FindObjectOfType<ReturnToMainMenu>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("collided");
        // if (other.CompareTag("CreditsEnd"))
        // {
            _returnToMainMenu.ToMainMenu();
        // }
    }
}
