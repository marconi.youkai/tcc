﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] private bool triggersOnlyOnce = false;
    private bool hasTriggered = false;
    public bool spawnCrouched = false;
    
    private void OnTriggerEnter(Collider other)
    {
        if ((!hasTriggered || !triggersOnlyOnce) && other.TryGetComponent(out PlayerCharacter p) && !p.seenByEnemy)
        {
            //print("pisou no Checkpoint " + Time.frameCount);
            //hasTriggered = true;
            //SaveDataManager.GetSave().savePosition = this.transform.position;
            //SaveDataManager.GetSave().spawnCrouched = spawnCrouched;
            //if (p.nerfGun != null)
            //    SaveDataManager.GetSave().hadNerfgun = true;
            //if (InventoryManager.inventoryManager.SearchForItem(ItemType.Cookie))
            //    SaveDataManager.GetSave().hadCookie = true;
            //SaveDataManager.GetSave().WriteSaveDataWithJson();
        }
    }
}
