﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableMaterialBlinker : MonoBehaviour
{
    [SerializeField] Material mat1;
    private Color mat1Color;
    [SerializeField] Material mat2;
    private Color mat2Color;
    private float intensityMultiplier = 1;
    private int inverter = 1;

    private void Start()
    {
        mat1Color = mat1.GetColor("_EmissionTint");
        mat2Color = mat2.GetColor("_EmissionTint");
        StartCoroutine(BlinkMaterials());
    }

    private IEnumerator BlinkMaterials()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            if (intensityMultiplier >= 1)
                inverter = -1;
            else if (intensityMultiplier <= 0)
                inverter = 1;
            intensityMultiplier += inverter * 1f * Time.fixedDeltaTime;
            mat1.SetColor("_EmissionTint", new Color(mat1Color.r * intensityMultiplier, mat1Color.g * intensityMultiplier, mat1Color.b * intensityMultiplier));
            mat2.SetColor("_EmissionTint", new Color(mat2Color.r * intensityMultiplier, mat2Color.g * intensityMultiplier, mat2Color.b * intensityMultiplier));
        }
    }

    private void OnDestroy()
    {
        mat1.SetColor("_EmissionTint", mat1Color);
        mat2.SetColor("_EmissionTint", mat2Color);
    }
    private void OnDisable()
    {
        mat1.SetColor("_EmissionTint", mat1Color);
        mat2.SetColor("_EmissionTint", mat2Color);

    }
}
