﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class EnemyCharacter : MonoBehaviour, ICharacter, IMouseOverable
{
    public float viewDistance;
    public float fovAngle = 90;
    public bool isSelected = false;
    protected NavMeshAgent agent;
    [SerializeField] [Range(1.5f, 4.24f)] protected float initialSpeed = 2.28f; //velocidade de chase fica min 3.8f max 7f
    protected List<IBuffDebuff> buffDebuffModifiers;
    protected float speedMultiplier = 0f;
    [SerializeField] TextMeshProUGUI textIndicator;
    public Transform targetTransform;

    //fazer uma lista de IInteractables pra checar. caso tenha e nao tenha alvo/escutando nada, vai até o interactable pra interagir no lugar de patrulhar (acender a luz, fechar torneira)

    [SerializeField] Transform head;

    public Transform Head()
    {
        return head;
    }

    protected VisualSensor _visualSensor;
    protected HearingSensor _hearingSensor;

    #region BuffDebuff Variables
    public float viewDistanceModifier = 1;
    protected float viewDistModTarget = 1;
    protected bool changingViewDist = false;

    public float fovAngleModifier = 1;
    protected float fovAngleModTarget = 1;
    protected bool changingFovAngle = false;

    protected float targetSpeed;
    protected bool changingSpeed = false;
    #endregion

    #region Detection Variables

    public bool hasTarget = false;
    public bool suspectsNoise = false;
    protected float visionReactionMultiplier = 1;
    protected float hearingReactionMultiplier = 1;
    [SerializeField] public float hearingReactionDelay = 2f;
    protected float hearingDelayTimer = 0.0f;
    [SerializeField] public float seeingReactionDelay = 0.3f;

    protected float seeingDelayTimer = 0.0f;
    //public bool canSee = false;
    //public bool canHear = false;

    #endregion

    //teste
    public int anglepersec = 60;

    //Finite State machine functions
    [SerializeField] protected GameObject player;
    public FSMSystem _fsm { get; protected set; }
    [SerializeField] protected Transform[] patrolWaypoints;

    [SerializeField] protected GameObject WaypointGroup;
    //public GameObject ChaseTarget;

    protected virtual void Start()
    {
        buffDebuffModifiers = new List<IBuffDebuff>();
        targetSpeed = initialSpeed;
        agent = GetComponent<NavMeshAgent>();
        agent.speed = initialSpeed;
        this.WaypointGroup.transform.SetParent(null);
        foreach (Transform waypoint in patrolWaypoints)
        {
            waypoint.SetParent(WaypointGroup.transform);
        }

        _hearingSensor = GetComponent<HearingSensor>();
        _visualSensor = GetComponent<VisualSensor>();

        MakeFSM();
        StealthDetectionManager.GetStealthDManager().RegisterEnemy(this);
        textIndicator.text = "";

        AkSoundEngine.RegisterGameObj(gameObject); //ao chamar evento de passo vai precisar, robo usa tb via chase state
    }

    public void SetTransition(Transition t)
    {
        _fsm.PerformTransition(t);
    }

    public void AddBuff(IBuffDebuff origin)
    {
        switch (origin.GetBuffType())
        {
            case buffDebuffType.speed:
                if (!buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Add(origin);
                    speedMultiplier += origin.GetFloatModifier();
                }

                RefreshMoveSpeed();
                break;
            case buffDebuffType.visionAndInverseFoV:
                if (!buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Add(origin);
                    viewDistModTarget += origin.GetFloatModifier();
                    if (!changingViewDist)
                        StartCoroutine(SmoothVisionRangeChange());
                    fovAngleModTarget -= origin.GetFloatModifier() * 0.85f;
                    if (!changingFovAngle)
                        StartCoroutine(SmoothFovChange());
                }
                break;
            case buffDebuffType.visionRange:
                if (!buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Add(origin);
                    viewDistModTarget += origin.GetFloatModifier();
                    if (!changingViewDist)
                        StartCoroutine(SmoothVisionRangeChange());
                }
                break;
            case buffDebuffType.fov:
                if (!buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Add(origin);
                    fovAngleModTarget -= origin.GetFloatModifier();
                    if (!changingFovAngle)
                        StartCoroutine(SmoothFovChange());
                }
                break;
            default:
                break;
        }
    }

    public void RemoveBuff(IBuffDebuff origin)
    {
        switch (origin.GetBuffType())
        {
            case buffDebuffType.speed:
                if (buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Remove(origin);
                    speedMultiplier -= origin.GetFloatModifier();
                }

                RefreshMoveSpeed();
                break;
            case buffDebuffType.visionAndInverseFoV:
                if (buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Remove(origin);
                    viewDistModTarget -= origin.GetFloatModifier();
                    if (!changingViewDist)
                        StartCoroutine(SmoothVisionRangeChange());
                    fovAngleModTarget += origin.GetFloatModifier() * 0.85f;
                    if (!changingFovAngle)
                        StartCoroutine(SmoothFovChange());
                }
                break;
            case buffDebuffType.visionRange:
                if (buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Remove(origin);
                    viewDistModTarget -= origin.GetFloatModifier();
                    if (!changingViewDist)
                        StartCoroutine(SmoothVisionRangeChange());
                }
                break;
            case buffDebuffType.fov:
                if (buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Remove(origin);
                    fovAngleModTarget += origin.GetFloatModifier();
                    if (!changingFovAngle)
                        StartCoroutine(SmoothFovChange());
                }
                break;
            default:
                break;
        }
    }

    private IEnumerator SmoothFovChange()
    {
        changingFovAngle = true;
        WaitForFixedUpdate t = new WaitForFixedUpdate();
        while (changingFovAngle)
        {
            float timeStep = 1 * Time.fixedDeltaTime;
            yield return t;
            fovAngleModifier = Mathf.MoveTowards(fovAngleModifier, fovAngleModTarget, timeStep);
            if (fovAngleModifier == fovAngleModTarget)
                changingFovAngle = false;
        }
    }

    private IEnumerator SmoothVisionRangeChange()
    {
        changingViewDist = true;
        WaitForFixedUpdate t = new WaitForFixedUpdate();
        while (changingViewDist)
        {
            float timeStep = 1 * Time.fixedDeltaTime;
            yield return t;
            viewDistanceModifier = Mathf.MoveTowards(viewDistanceModifier, viewDistModTarget, timeStep);
            if (viewDistanceModifier == viewDistModTarget)
                changingViewDist = false;
        }
    }

    protected void RefreshMoveSpeed()
    {
        targetSpeed = initialSpeed * (1 + speedMultiplier);
        if (!changingSpeed)
            StartCoroutine(ChangingTargetSpeed());
    }

    protected IEnumerator ChangingTargetSpeed()
    {
        changingSpeed = true;
        WaitForFixedUpdate t = new WaitForFixedUpdate();
        while (changingSpeed)
        {
            float timeStep = 20 * Time.fixedDeltaTime;
            yield return t;
            agent.speed = Mathf.MoveTowards(agent.speed, targetSpeed, timeStep);
            if (agent.speed == targetSpeed)
                changingSpeed = false;
        }
    }

    public void EnemyUpdate(List<PlayerCharacter> pList)
    {
        PlayerCharacter nearestPlayer = null;
        float playerDistance = 0;

        foreach (var player in pList)
        {
            var playerDist = (this.transform.position - player.transform.position).sqrMagnitude;
            if (playerDistance <= 0 || playerDistance > playerDist)
            {
                if (!player.insideSafeZone)
                {
                    playerDistance = playerDist;
                    nearestPlayer = player;
                }
            }
        }

        var canHear = _hearingSensor.Detected(this, nearestPlayer);
        var canSee = _visualSensor.Detected(this, nearestPlayer);

        //nao sei se é o melhor lugar, mas tudo isso aqui é o delay pra adquirir o alvo/ouvir
        if (!hasTarget && canSee)
            seeingDelayTimer += Time.fixedDeltaTime * visionReactionMultiplier;
        else if (!canSee)
            if (seeingDelayTimer > 0)
                seeingDelayTimer -= Time.fixedDeltaTime;
            else
                seeingDelayTimer = 0;
        if (!suspectsNoise && canHear)
            hearingDelayTimer += Time.fixedDeltaTime * (1 + (nearestPlayer.noiseLevel - Vector3.Distance(transform.position, nearestPlayer.transform.position)) / 2) * hearingReactionMultiplier; //conta maluca, testar pra ver o que acontece. a ideia é que se o barulho for muito alto, o inimigo vai reagir mais rápido (por isso a reação default é 2 segundos)
        else if (!canHear)
            if (hearingDelayTimer > 0)
                hearingDelayTimer -= Time.fixedDeltaTime;
            else
                hearingDelayTimer = 0;

        if (seeingDelayTimer >= seeingReactionDelay)
            hasTarget = true;
        else if (seeingDelayTimer <= 0 && !canSee)
            hasTarget = false;

        if (hearingDelayTimer >= hearingReactionDelay)
        {
            suspectsNoise = true;
        }
        else if (hearingDelayTimer <= 0 && !canHear)
            suspectsNoise = false;

        //if (nearestPlayer == null)
        //{
        //    hasTarget = false;
        //    suspectsNoise = false;
        //}     

        _fsm.CurrentState.Reason(nearestPlayer);
        _fsm.CurrentState.Act(nearestPlayer);

        if (nearestPlayer == null)
            return;
        //provisorio (informar o player se tá fazendo barulho audível ou tá dentro do campo de visão. Antes do inimigo efetivamente perceber).
        if (canSee)
            nearestPlayer.visibleBy(this);
        else
        {
            nearestPlayer.notVisibleBy(this);
            if (canHear)
                nearestPlayer.heardBy(this);
            else
                nearestPlayer.notHeardBy(this);
        }
    }

    public void EnemyUpdate(PlayerCharacter p)
    {
        var _canHear = _hearingSensor.Detected(this, p);
        var _canSee = _visualSensor.Detected(this, p);

        //nao sei se é o melhor lugar, mas tudo isso aqui é o delay pra adquirir o alvo/ouvir
        if (!hasTarget && _canSee)
            seeingDelayTimer += Time.fixedDeltaTime * visionReactionMultiplier;
        else if (!_canSee)
            if (seeingDelayTimer > 0)
                seeingDelayTimer -= Time.fixedDeltaTime;
            else
                seeingDelayTimer = 0;
        if (!suspectsNoise && _canHear)
            hearingDelayTimer += Time.fixedDeltaTime * (1 + (p.noiseLevel - Vector3.Distance(transform.position, p.transform.position)) / 2) * hearingReactionMultiplier; //conta maluca, testar pra ver o que acontece. a ideia é que se o barulho for muito alto, o inimigo vai reagir mais rápido (por isso a reação default é 2 segundos)
        else if (!_canHear)
            if (hearingDelayTimer > 0)
                hearingDelayTimer -= Time.fixedDeltaTime;
            else
                hearingDelayTimer = 0;

        if (seeingDelayTimer >= seeingReactionDelay)
            hasTarget = true;
        else if (seeingDelayTimer <= 0 && !_canSee)
            hasTarget = false;

        if (hearingDelayTimer >= hearingReactionDelay)
        {
            suspectsNoise = true;
        }
        else if (hearingDelayTimer <= 0 && !_canHear)
            suspectsNoise = false;

        _fsm.CurrentState.Reason(p);
        _fsm.CurrentState.Act(p);

        if (p == null)
            return;
        //provisorio (informar o player se tá fazendo barulho audível ou tá dentro do campo de visão. Antes do inimigo efetivamente perceber).
        if (_canSee)
            p.visibleBy(this);
        else
        {
            p.notVisibleBy(this);
            if (_canHear)
                p.heardBy(this);
            else
                p.notHeardBy(this);
        }
    }

    protected void OnValidate()
    {
        if (WaypointGroup == null)
        {
            WaypointGroup = new GameObject("WaypointGroup");
            WaypointGroup.transform.SetParent(this.transform);
        }

        if (patrolWaypoints.Length <= 0) return;

        for (int i = 0; i < patrolWaypoints.Length; i++)
        {
            if (patrolWaypoints[i] != null)
            {
                return;
            }

            var wp = new GameObject("Waypoint " + i);
            wp.transform.SetParent(WaypointGroup.transform);
            wp.transform.localPosition = Vector3.zero;
            wp.transform.rotation = Quaternion.identity;
            patrolWaypoints[i] = wp.GetComponent<Transform>();
        }
    }

    public void Select()
    {
        if (StealthDetectionManager.selectedEnemySingleton != null)
            StealthDetectionManager.selectedEnemySingleton.isSelected = false;
        isSelected = true;
        StealthDetectionManager.selectedEnemySingleton = this;
        //print("selected " + StealthDetectionManager.selectedEnemySingleton.name);
    }

    public void Deselect()
    {
        if (!isSelected)
            return;
        StealthDetectionManager.selectedEnemySingleton = null;
        isSelected = false;
    }

    public void EnterAlertState()
    {
        textIndicator.text = "?";
        visionReactionMultiplier = 2f;
        hearingReactionMultiplier = 1.4f;
    }

    public void EnterChaseState()
    {
        textIndicator.text = "!";
        visionReactionMultiplier = 2.5f;
        hearingReactionMultiplier = 1.2f;
    }

    public void LeaveEmergencyState()
    {
        textIndicator.text = "";
        visionReactionMultiplier = 1;
        hearingReactionMultiplier = 1;
    }

    protected virtual void MakeFSM()
    {
        //print("fsm velho");
        PatrolState patrol = new PatrolState(agent, patrolWaypoints, this);
        patrol.AddTransition(Transition.SawPlayer, StateID.Chasing);
        patrol.AddTransition(Transition.HeardPlayer, StateID.Listening);

        ChaseState chase = new ChaseState(agent, this); //tirei chasetarget
        chase.AddTransition(Transition.HeardPlayer, StateID.Listening);
        chase.AddTransition(Transition.AttackPlayer, StateID.Attacking);

        AttackState attacking = new AttackState(agent, this); //tirei chasetarget
        attacking.AddTransition(Transition.LostPlayer, StateID.Patroling);
        attacking.AddTransition(Transition.HeardPlayer, StateID.Listening);
        attacking.AddTransition(Transition.SawPlayer, StateID.Chasing);

        ListeningState listening = new ListeningState(agent, this);
        listening.AddTransition(Transition.SawPlayer, StateID.Chasing);
        listening.AddTransition(Transition.LostPlayer, StateID.Patroling);


        _fsm = new FSMSystem();
        _fsm.AddState(patrol);
        _fsm.AddState(chase);
        _fsm.AddState(attacking);
        _fsm.AddState(listening);
    }

    public string MouseOverTooltip()
    {
        return "Right-Click to display this unit's field of view";
    }

    public CursorManager.CMStates[] MouseOverStatesAllowed()
    {
        return new CursorManager.CMStates[1] { CursorManager.CMStates.InGame };
    }

    public CursorManager.CursorType MouseOverCursorType()
    {
        return CursorManager.CursorType.Rightclick;
    }
}