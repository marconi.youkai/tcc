﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRobozin : EnemyCharacter
{
    protected override void Start()
    {
        base.Start();
    }
    protected override void MakeFSM()
    {
        PatrolState patrol = new PatrolState(agent, patrolWaypoints, this);
        patrol.AddTransition(Transition.SawPlayer, StateID.Chasing);
        patrol.AddTransition(Transition.HeardPlayer, StateID.Listening);

        RobotChaseState chase = new RobotChaseState(agent, this); //tirei chasetarget
        chase.AddTransition(Transition.HeardPlayer, StateID.Listening);
        chase.AddTransition(Transition.AttackPlayer, StateID.Attacking);

        RobotAttackState attacking = new RobotAttackState(agent, this); //tirei chasetarget
        attacking.AddTransition(Transition.LostPlayer, StateID.Patroling);
        attacking.AddTransition(Transition.HeardPlayer, StateID.Listening);
        attacking.AddTransition(Transition.SawPlayer, StateID.Chasing);

        ListeningState listening = new ListeningState(agent, this);
        listening.AddTransition(Transition.SawPlayer, StateID.Chasing);
        listening.AddTransition(Transition.LostPlayer, StateID.Patroling);


        _fsm = new FSMSystem();
        _fsm.AddState(patrol);
        _fsm.AddState(chase);
        _fsm.AddState(attacking);
        _fsm.AddState(listening);
    }
    private void OnDestroy()
    {
        AkSoundEngine.PostEvent("Robot_Chase_End", gameObject);
    }
}
