﻿using UnityEngine;
using UnityEngine.AI;

public class RobotAttackState : FSMState
{
    private NavMeshAgent _agent;
    private EnemyCharacter e;

    public RobotAttackState(NavMeshAgent agent, EnemyCharacter _e)
    {
        this._agent = agent;
        stateID = StateID.Attacking;
        e = _e;
    }

    public override void Reason(PlayerCharacter p)
    {
        if (p == null)
        {
            e.SetTransition(Transition.HeardPlayer);
            return;
        }

        if (Vector3.Distance(p.transform.position, e.transform.position) > 1.5f)
        {
            e.SetTransition(Transition.SawPlayer);
        }
        if (Vector3.Distance(p.transform.position, e.transform.position) <= 1.05f)
            p.LoseTutorial();

        if (!e.hasTarget)
        {
            e.SetTransition(Transition.LostPlayer);
        }
    }

    public override void DoBeforeEntering()
    {
        _agent.velocity = Vector3.zero;
    }

    public override void Act(PlayerCharacter p)
    {
        Quaternion temp;
        if (p != null)
            temp = Quaternion.LookRotation(p.lastKnownLocation - e.transform.position);
        else
            temp = e.transform.rotation;
        Quaternion temp2 = Quaternion.Euler(e.transform.rotation.eulerAngles.x, temp.eulerAngles.y, e.transform.rotation.eulerAngles.z);
        e.transform.rotation = Quaternion.RotateTowards(e.transform.rotation, temp2, 180.0f * Time.fixedDeltaTime);
    }
}