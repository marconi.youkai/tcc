using UnityEngine;
using UnityEngine.AI;

public class RobotChaseState : ChaseState//FSMState, IBuffDebuff
{
    //private NavMeshAgent _agent;
    //private Transform playerPosition;
    //private EnemyCharacter e;
    //private float movespeedBuff = 0.67f;
    //private buffDebuffType buffType = buffDebuffType.speed;

    public RobotChaseState(NavMeshAgent agent, EnemyCharacter _e) : base(agent, _e)
    {
        this._agent = agent;
        stateID = StateID.Chasing;
        e = _e;
    }

    public override void DoBeforeEntering()
    {
        //e.EnterChaseState();
        //e.AddBuff(this);
        base.DoBeforeEntering();
        AkSoundEngine.PostEvent("Robot_Chase", e.gameObject);
    }

    public override void DoBeforeLeaving()
    {
        //e.LeaveEmergencyState();
        //e.RemoveBuff(this);
        base.DoBeforeLeaving();
        AkSoundEngine.PostEvent("Robot_Chase_End", e.gameObject);
    }

    //public override void Reason(PlayerCharacter p)
    //{
    //    if (_agent.remainingDistance < 1.0f)
    //    {
    //        if (!e.hasTarget)
    //        {
    //            e.SetTransition(Transition.HeardPlayer);
    //        }
    //        else
    //        {
    //            _agent.velocity = Vector3.zero;
    //            e.SetTransition(Transition.AttackPlayer);
    //        }
    //    }
    //}

    //public override void Act(PlayerCharacter p)
    //{
    //    if (p != null)
    //        _agent.SetDestination(p.lastKnownLocation);
    //}

    //public float GetFloatModifier()
    //{
    //    return movespeedBuff;
    //}

    //public buffDebuffType GetBuffType()
    //{
    //    return buffType;
    //}
}