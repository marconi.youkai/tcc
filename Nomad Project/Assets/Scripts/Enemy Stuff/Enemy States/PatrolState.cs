using UnityEngine;
using UnityEngine.AI;

public class PatrolState : FSMState
{
    private NavMeshAgent _agent;
    private int currentWaypoint = 0;
    private Transform[] waypoints;
    private EnemyCharacter e;

    public PatrolState(NavMeshAgent agent, Transform[] wp, EnemyCharacter _e)
    {
        this._agent = agent;
        this.waypoints = wp;
        this.currentWaypoint = 0;
        stateID = StateID.Patroling;
        e = _e;
    }

    public override void DoBeforeEntering()
    {
        _agent.isStopped = false;
        _agent.SetDestination(waypoints[currentWaypoint].position);
    }

    public override void Reason(PlayerCharacter p)
    {
        if (e.hasTarget)
        {
            e.SetTransition(Transition.SawPlayer);
        }
        else if (e.suspectsNoise)
        {
            e.SetTransition(Transition.HeardPlayer);
        }
    }

    public override void Act(PlayerCharacter p)
    {
        if (_agent.remainingDistance < 0.5f)
        {
            currentWaypoint = ++currentWaypoint % (waypoints.Length);
            _agent.SetDestination(waypoints[currentWaypoint].position);
        }
    }
}