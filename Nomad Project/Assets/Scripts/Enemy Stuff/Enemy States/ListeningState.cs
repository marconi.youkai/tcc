using UnityEngine;
using UnityEngine.AI;

public class ListeningState : FSMState, IBuffDebuff
{
    private NavMeshAgent agent;
    private float giveUpTimer = 3.0f;
    private EnemyCharacter e;
    private float lookAroundTimer = 0.0f;
    private Vector3 lastKnownLocation;
    private Quaternion currentLookTarget;

    public ListeningState(NavMeshAgent _agent, EnemyCharacter _e)
    {
        agent = _agent;
        stateID = StateID.Listening;
        e = _e;
    }

    public override void DoBeforeEntering()
    {
        giveUpTimer = 3.0f;
        e.AddBuff(this);
        e.EnterAlertState();
        agent.isStopped = true;
        //agent.stoppingDistance = 1000f;
        lookAroundTimer = Random.Range(0.25f, 0.4f);
    }

    public override void DoBeforeLeaving()
    {
        e.LeaveEmergencyState();
        e.RemoveBuff(this);
        agent.isStopped = false;
        agent.stoppingDistance = 0.5f;
    }

    public override void Reason(PlayerCharacter p)
    {
        giveUpTimer -= Time.fixedDeltaTime;

        if (giveUpTimer <= 0.0f && !e.suspectsNoise)
        {
            e.SetTransition(Transition.LostPlayer);
        }

        if (e.hasTarget)
        {
            e.SetTransition(Transition.SawPlayer);
        }
    }

    public override void Act(PlayerCharacter p)
    {
        lookAroundTimer -= Time.fixedDeltaTime;
        if (lookAroundTimer <= 0)
        {
            lookAroundTimer += Random.Range(0.4f, 1.2f);
            Vector3 temp;
            if (p != null)
                temp = p.lastHeardLocation - e.transform.position;
            else
                temp = e.transform.position + e.transform.forward;
            Quaternion temp2 = Quaternion.LookRotation(temp);
            currentLookTarget = Quaternion.Euler(e.transform.rotation.eulerAngles.x, temp2.eulerAngles.y + Random.Range(-70, 70), e.transform.rotation.eulerAngles.z);
        }

        e.transform.rotation = Quaternion.RotateTowards(e.transform.rotation, currentLookTarget, 270f * Time.fixedDeltaTime);
    }

    public buffDebuffType GetBuffType()
    {
         return buffDebuffType.visionAndInverseFoV;
    }

    public float GetFloatModifier()
    {
        return 0.2f;
    }
}