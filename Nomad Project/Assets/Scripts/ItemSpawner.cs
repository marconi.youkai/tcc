﻿using UnityEngine;

public class ItemSpawner : MonoBehaviour, IInteractable<ICharacter>, IMouseOverable
{
    // só se pode interagir com esse tipo de objeto uma vez.
    [SerializeField] private int totalCharges = 0;
    private int charges = 0;
    [SerializeField] private GameObject fullVersion;
    [SerializeField] private GameObject halfVersion;
    [SerializeField] private GameObject emptyVersion;

    [Header("Item Spawnado")]
    [SerializeField] private GameObject[] inventoryIconPrefab;

    [SerializeField] private GameObject[] inGameModelPrefab;
    [SerializeField] private ItemType spawnedItemType;
    [SerializeField] private string description;
    [SerializeField] private string spawnedDescription;
    [SerializeField] private int marketValue;
    [SerializeField] private int level;
    [SerializeField] private float noiseOnInteraction;
    private AudioEvent audioEvent;

    private bool unclickable = false;
    private Item item;


    private void Start()
    {
        item = new Item(ItemType.Cookie, spawnedDescription, marketValue, level, inventoryIconPrefab[0], null, true, noiseOnInteraction);
        charges = totalCharges;
        audioEvent = GetComponent<AudioEvent>();
    }

    public bool Interact(ICharacter character)
    {
        if (unclickable) return false;
        Obtain();
        ((PlayerCharacter)character).GenerateNonMovementNoise(noiseOnInteraction);
        audioEvent.player = (PlayerCharacter)character;
        audioEvent.Step();
        return true;
    }

    public Item ReadItem()
    {
        return item;
    }

    public void Obtain()
    {
        if (charges == totalCharges)
        {
            fullVersion.SetActive(false);
            halfVersion.SetActive(true);
        }

        charges--;
        if (charges <= 0)
        {
            halfVersion.SetActive(false);
            emptyVersion.SetActive(true);
            unclickable = true;
        }

        int i = Random.Range(0, inventoryIconPrefab.Length);
        GameObject go = Instantiate(inventoryIconPrefab[i]);
        InventoryItem temp = go.GetComponent<InventoryItem>();
        item.InventoryIconPrefab = inventoryIconPrefab[i];
        //linha para atualizar o ingameprefab também (por enquanto nulo, até termos habilidade de dropar no chao).. uma gaveta de talheres q spawna garfos e facas por ex. mesma função ingame (tacar) mas tem q ser consistente no inventário e no chao
        temp.Item = item;
        InventoryManager.GetInventoryManager().Inventory.InsertItem(temp);
    }

    public string MouseOverTooltip()
    {
        return description;
    }

    public CursorManager.CMStates[] MouseOverStatesAllowed()
    {
        return new CursorManager.CMStates[1] { CursorManager.CMStates.InGame };
    }

    public CursorManager.CursorType MouseOverCursorType()
    {
        MouseOverRaycaster.Instance.StartContinuousRefresh();
        if (Vector3.Distance(InventoryManager.GetInventoryManager().activePlayer.transform.position, transform.position) <= 1.4f)
            return CursorManager.CursorType.Rightclick;
        else if (!unclickable)
            return CursorManager.CursorType.Unavailable;
        else
            return CursorManager.CursorType.Default;
    }
}