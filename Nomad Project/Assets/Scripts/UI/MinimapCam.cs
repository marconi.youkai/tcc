﻿using UnityEngine;

public class MinimapCam : MonoBehaviour
{
    private Transform player;
    private Camera cam;
    [SerializeField] private int zoomSteps = 9;
    private int currentZoom;
    [SerializeField] private float minZoomSize;
    [SerializeField] private float maxZoomSize;
    private void Start()
    {
        cam = GetComponent<Camera>();
        player = FindObjectOfType<PlayerCharacter>().transform; //provisorio
        currentZoom = Mathf.CeilToInt(zoomSteps / 2f);
        AdjustCameraZoom(0);
    }
    private void Update()
    {
        transform.position = new Vector3(player.position.x, 7, player.position.z);
    }
    public void UpdatePlayer(Transform _player)
    {
        player = _player;
    }

    public void PressPlus()
    {
        AdjustCameraZoom(-1);
    }
    public void PressMinus()
    {
        AdjustCameraZoom(1);
    }
    private void AdjustCameraZoom(int n)
    {
        currentZoom = Mathf.Clamp(currentZoom + n, 0, zoomSteps - 1);
        cam.orthographicSize = ((maxZoomSize - minZoomSize) / (zoomSteps - 1) * currentZoom) + minZoomSize;
    }
}
