﻿using UnityEngine;

namespace DialogSystem
{
    [CreateAssetMenu(fileName = "New Question", menuName = "Dialog system/New question", order = 2)]
    public class Question : ScriptableObject
    {
        [SerializeField, InspectorName("Questioning Character")]
        private UICharacter character;

        [SerializeField] private string questionText;
        [SerializeField] private Option[] possibleOptions = new Option[2];
        [SerializeField] public Option[] PossibleOptions => possibleOptions;
        public string QuestionText => questionText;

        [System.Serializable]
        public struct Option
        {
            [SerializeField] public string optionText;
            [SerializeField] public Dialog followUpDialog;
            public Dialog FollowUpDialog => followUpDialog;
        }
    }
}