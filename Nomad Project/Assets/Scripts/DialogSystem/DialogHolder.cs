﻿using UnityEngine;


namespace DialogSystem
{
    public class DialogHolder : MonoBehaviour
    {
        [SerializeField] private Dialog _dialog;
        private DialogController _dialogController;

        private void Awake()
        {
            _dialogController = FindObjectOfType<DialogController>();
        }

        public void AssignDialog()
        {
            if (_dialogController == null)
            {
                _dialogController = FindObjectOfType<DialogController>();
            }

            _dialogController.StartDialog(_dialog);
        }
    }
}