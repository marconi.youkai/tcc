﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace DialogSystem
{
    public class DialogController : MonoBehaviour
    {
        [SerializeField] private GameObject _dialogUi;
        [SerializeField] private GameObject _questionUi;
        [SerializeField] private TextMeshProUGUI _avatarNameTxtBox;
        [SerializeField] private TextMeshProUGUI _dialogTxtBox;
        [SerializeField] private Image _avatarImg;
        [SerializeField] private Button _optionButton1, _optionButton2;
        [SerializeField] private float letterDelay = 0.005f;
        [SerializeField] private GameObject[] inGameUI;
        public bool DialogActive { get; private set; }
        private TextMeshProUGUI _Button1Text, _Button2Text;
        private Dialog _dialog = null;
        private float charElapsedTime = 0;
        private int _dialogIndex = 0;
        private int _dialogStep = 0;
        [SerializeField] private UnityEvent[] events;

        private Coroutine _currentCoroutine;

        private void Start()
        {
            _dialogUi.SetActive(false);
            _questionUi.SetActive(false);
            _Button1Text = _optionButton1.GetComponentInChildren<TextMeshProUGUI>();
            _Button2Text = _optionButton2.GetComponentInChildren<TextMeshProUGUI>();
        }

        private void UpdateDialog()
        {
            _avatarNameTxtBox.text = _dialog.DialogBoxes[_dialogIndex].characterName;
            _avatarImg.sprite = _dialog.DialogBoxes[_dialogIndex].characterImg;
            _currentCoroutine = StartCoroutine(DialogLetterFiller(_dialog.DialogBoxes[_dialogIndex].dialog));
        }

        public void StartDialog(Dialog dialog)
        {
            if (DialogActive)
                return;
            ToggleIngameUI(false);
            CursorManager.GetCursorManager().StackState(CursorManager.CMStates.DialogLock);
            DialogActive = true;
            this._dialogIndex = 0;
            this._dialog = dialog;

            _questionUi.SetActive(false);
            _optionButton1.onClick.RemoveAllListeners();
            _optionButton2.onClick.RemoveAllListeners();
            _dialogUi.SetActive(true);
            UpdateDialog();
        }

        public void ToggleIngameUI(bool show)
        {
            if (show)
            {
                foreach (GameObject go in inGameUI)
                {
                    go.SetActive(true);
                }
            }
            else
            {
                foreach (GameObject go in inGameUI)
                {
                    go.SetActive(false);
                }
            }
        }

        public void EndDialog()
        {
            if (_dialog.EndInQuestion)
            {
                ShowQuestion();
                this._dialogIndex = 0;
                return;
            }

            ToggleIngameUI(true);
            CursorManager.GetCursorManager().LeaveState(CursorManager.CMStates.DialogLock);
            DialogActive = false;
            _dialogUi.SetActive(false);
            this._dialogIndex = 0;
        }

        public void NextDialogLine()
        {
            if (_currentCoroutine != null)
            {
                StopCoroutine(_currentCoroutine);
                _currentCoroutine = null;
                _dialogTxtBox.text = _dialog.DialogBoxes[_dialogIndex].dialog;
                return;
            }
            
            if (_dialog.DialogBoxes[_dialogIndex].DialogControllerEventIndex != 0)
                events[_dialog.DialogBoxes[_dialogIndex].DialogControllerEventIndex - 1].Invoke();
            if (_dialogIndex >= _dialog.DialogBoxes.Length - 1)
            {
                EndDialog();
            }
            else
            {
                _dialogIndex++;
                UpdateDialog();
            }
        }

        private void ShowQuestion()
        {
            _dialogTxtBox.text = _dialog.Question.QuestionText;
            _Button1Text.text = _dialog.Question.PossibleOptions[0].optionText;
            _Button2Text.text = _dialog.Question.PossibleOptions[1].optionText;

            _optionButton1.onClick.AddListener(delegate { StartDialog(_dialog.Question.PossibleOptions[0].FollowUpDialog); });
            _optionButton2.onClick.AddListener(delegate { StartDialog(_dialog.Question.PossibleOptions[1].FollowUpDialog); });

            _questionUi.SetActive(true);
        }

        /// <summary>
        /// Coroutine to progressively write into dialog boxes letter by letter
        /// </summary>
        /// <param name="dialog">Dialog to be written</param>
        /// <returns></returns>
        private IEnumerator DialogLetterFiller(string dialog)
        {
            var charIndex = 0;

            while (charIndex <= dialog.Length)
            {
                _dialogTxtBox.text = dialog.Substring(0, charIndex);
                charIndex++;
                yield return new WaitForSeconds(letterDelay);
            }

            _currentCoroutine = null;
        }
    }
}