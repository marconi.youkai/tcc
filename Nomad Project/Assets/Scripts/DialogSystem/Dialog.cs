﻿using UnityEngine;

namespace DialogSystem
{
    [CreateAssetMenu(fileName = "New Dialog", menuName = "Dialog system/New dialog", order = 1)]
    public class Dialog : ScriptableObject
    {
        private bool _endInQuestion = false;

        [SerializeField, InspectorName("Chatting Characters")]
        private UICharacter[] _characters = new UICharacter[0];

        [SerializeField] private DialogBox[] dialogBoxes = new DialogBox[0];

        [SerializeField, InspectorName("End question")]
        private Question _question = null;

        public UICharacter[] Characters => _characters;
        public DialogBox[] DialogBoxes => dialogBoxes;

        public Question Question => _question;

        public bool EndInQuestion
        {
            get => _endInQuestion;
            private set => _endInQuestion = value;
        }

        private void OnValidate()
        {
            EndInQuestion = _question != null;
        }

        [System.Serializable]
        public struct DialogBox
        {
            [SerializeField] public UICharacter character;
            public string characterName => character.Name;
            public Sprite characterImg => character.Image;
            [TextArea(3, 10)] public string dialog;
            [SerializeField] public int DialogControllerEventIndex;
        }
    }
}