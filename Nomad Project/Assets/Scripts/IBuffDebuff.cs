﻿public enum buffDebuffType { speed, visionRange, fov, visionAndInverseFoV, sensToSound, };
public interface IBuffDebuff
{
    buffDebuffType GetBuffType();
    float GetFloatModifier();
}