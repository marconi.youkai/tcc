﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.Universal;
using UnityEngine.InputSystem;

public class PauseMenuController : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuPanel;
    [SerializeField] private GameObject optionsPanel;
    [SerializeField] private string mainMenuSceneName;
    [SerializeField] private UniversalRenderPipelineAsset lowQualityAsset;
    [SerializeField] private UniversalRenderPipelineAsset averageQualityAsset;
    [SerializeField] private UniversalRenderPipelineAsset highQualityAsset;
    int counter = 0;

    private void Awake()
    {
        pauseMenuPanel.SetActive(false);
        optionsPanel.SetActive(false);
    }

    public void OnPressEsc(InputAction.CallbackContext context) //se nao colocar esse context ele funciona, mas chama o método 3 vezes (InputActionPhase.Started, Waiting e Performed acho)
    {
        if (!context.performed) //aqui garantimos que só roda uma vez, no performed
            return;

        if (!pauseMenuPanel.activeSelf)
        {
            if (optionsPanel.activeSelf)
            {
                OnButtonBack();
                return;
            }
            OnOpenMenu();
        }
        else
            OnButtonExitMenu();
    }

    private void OnOpenMenu()
    {
        pauseMenuPanel.SetActive(true);
        Time.timeScale = 0;
        CursorManager.instance.StackState(CursorManager.CMStates.GamePaused);
    }

    public void OnButtonMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(mainMenuSceneName);
    }

    public void OnButtonQuit()
    {
        Application.Quit();
    }

    public void OnButtonOptions()
    {
        pauseMenuPanel.SetActive(false);
        optionsPanel.SetActive(true);
    }

    public void OnMusicSlider(Slider soundSlider)
    {
        AudioListener.volume = soundSlider.value;
    }

    public void OnAudioSlider(Slider soundSlider)
    {
        // TODO: Mudar volume do WWise
    }

    public void OnQualitySlider(Slider qualitySlider)
    {
        switch ((int)qualitySlider.value)
        {
            case 1:
                if (lowQualityAsset == null) return;
                GraphicsSettings.renderPipelineAsset = lowQualityAsset;
                break;
            case 2:
                if (averageQualityAsset == null) return;
                GraphicsSettings.renderPipelineAsset = averageQualityAsset;
                break;
            case 3:
                if (highQualityAsset == null) return;
                GraphicsSettings.renderPipelineAsset = highQualityAsset;
                break;
            default:
                break;
        }
    }

    public void OnButtonBack()
    {
        optionsPanel.SetActive(false);
        pauseMenuPanel.SetActive(true);
    }

    public void OnButtonExitMenu()
    {
        Time.timeScale = 1;
        pauseMenuPanel.SetActive(false);
        optionsPanel.SetActive(false);
        CursorManager.instance.LeaveState(CursorManager.CMStates.GamePaused);
    }
}
