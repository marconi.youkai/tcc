﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class LightFlicker : MonoBehaviour
{
    private Light lightObj;
    private float intensity;
    private float Temperature;
    private bool lightsOn => lightObj.enabled;
    [SerializeField, Range(0.1f, 1000f)] float lightIntensityMin = 300f;
    [SerializeField, Range(0.2f, 1000f)] float lightIntensityMax = 800f;

    [SerializeField, Range(0.0001f, 0.9999f)]
    float lightIntensityTransitionSmoothness = 0.1f;

    [SerializeField] private Color[] lightColor;

    private void Awake()
    {
        try
        {
            this.lightObj = this.GetComponent<Light>();
        }
        finally { }
        if (lightColor.Length != 0)
            StartCoroutine(colorUpdater());
        intensity = Random.Range(lightIntensityMin, lightIntensityMax);
    }


    void FixedUpdate()
    {
        if (lightsOn)
        {
            intensity = Random.Range(lightIntensityMin, lightIntensityMax);
            lightObj.intensity = Mathf.Lerp(lightObj.intensity, intensity, lightIntensityTransitionSmoothness);
        }
    }

    public void LightSwitch()
    {
        lightObj.enabled = !lightsOn;
    }

    private IEnumerator colorUpdater()
    {
        if (!lightsOn)
            yield return null;
        //if (lightColor.Length == 0)
        //    yield break;
        Color randomColor = lightColor[Random.Range(0, lightColor.Length)];
        float randomTime = Random.Range(.5f, 2f);

        while (lightObj.color != randomColor)
        {
            lightObj.color = Color.Lerp(lightObj.color, randomColor, Random.Range(0.1f, 0.4f));
            yield return null;
        }

        yield return new WaitForSeconds(randomTime);
        StartCoroutine(colorUpdater());
    }
}