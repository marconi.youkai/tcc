﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private Transform leftDoor;
    [SerializeField] [Range(0.01f, 1)] private float leftDoorAdjustmentMultiplier = 1;
    [SerializeField] private Transform rightDoor;
    [SerializeField] [Range(0.01f, 1)] private float rightDoorAdjustmentMultiplier = 1;
    public bool isOpen { get; private set; } = false;
    public bool isLocked { get; private set; } = false;
    [SerializeField] GameObject[] enabledGOs;
    [SerializeField] GameObject[] disabledGOs;

    private bool currentlyMoving = false;
    private float drivingFloat;

    private void Start()
    {
        AkSoundEngine.RegisterGameObj(gameObject);
    }

    public void SwitchDoor()
    {
        if (isLocked || currentlyMoving)
            return;

        AkSoundEngine.PostEvent("Play_lightswitch", gameObject);
        isOpen = !isOpen;
        if (isOpen)
        {
            StartCoroutine(OpenDoor());
        }
        else
        {
            StartCoroutine(CloseDoor());
        }
    }

    private IEnumerator OpenDoor()
    {
        foreach (GameObject go in enabledGOs)
        {
            go.SetActive(true);
        }
        foreach (GameObject go in disabledGOs)
        {
            go.SetActive(false);
        }
        AkSoundEngine.PostEvent("Play_Kitchen_Door_Open", gameObject);
        currentlyMoving = true;
        float outputFloat = 0f;
        drivingFloat = 0.001f;
        while (drivingFloat < 9.5f)
        {
            outputFloat = 2 * Mathf.Sin(drivingFloat) / drivingFloat * 4 - 8;
            rightDoor.localPosition = new Vector3(-1f * outputFloat * rightDoorAdjustmentMultiplier, 0, 0);
            leftDoor.localPosition = new Vector3(1f * outputFloat * leftDoorAdjustmentMultiplier, 0, 0);
            drivingFloat += 10f * Time.deltaTime;
            yield return null;
        }
        currentlyMoving = false;
    }

    private IEnumerator CloseDoor()
    {
        AkSoundEngine.PostEvent("Play_Kitchen_Door_Close", gameObject);
        currentlyMoving = true;
        float outputFloat = 0f;
        drivingFloat = 9.5f;
        while (drivingFloat > 0f)
        {
            outputFloat = 2 * Mathf.Sin(drivingFloat) / drivingFloat * 4 - 8;
            rightDoor.localPosition = new Vector3(-1 * outputFloat * rightDoorAdjustmentMultiplier, 0, 0);
            leftDoor.localPosition = new Vector3(1 * outputFloat * leftDoorAdjustmentMultiplier, 0, 0);
            drivingFloat -= 14f * Time.deltaTime;
            yield return null;
        }
        rightDoor.localPosition = Vector3.zero;
        leftDoor.localPosition = Vector3.zero;
        currentlyMoving = false;
        foreach (GameObject go in enabledGOs)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in disabledGOs)
        {
            go.SetActive(true);
        }
    }

    public void LockUnlockDoor(bool locking)
    {
        isLocked = locking;
    }
}
