﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOSwapper : MonoBehaviour
{
    [SerializeField] private GameObject mainGameObject;
    [SerializeField] private GameObject altGameObject;
    [SerializeField] private bool startWithAlternative;

    private void Start()
    {
        mainGameObject.SetActive(!startWithAlternative);
        altGameObject.SetActive(startWithAlternative);
    }

    public void Switch()
    {
        mainGameObject.SetActive(altGameObject.activeSelf);
        altGameObject.SetActive(!mainGameObject.activeSelf);
    }
}
