﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using CameraSystem;

[RequireComponent(typeof(Inventory))]
public class PlayerCharacter : MonoBehaviour, ICharacter
{
    private Inventory _inventory;
    public Inventory _Inventory => _inventory;
    public void SetInventory(Inventory inv) { _inventory = inv; }

    public bool isCrouching { get; private set; } = false;
    public bool seenByEnemy { get; private set; } = false;
    public bool heardByEnemy { get; private set; } = false;
    public bool isRunning { get; private set; } = false;
    public bool isAiming = false;
    public bool insideSafeZone = false;
    public int characterIndex = 0;
    
    private bool controllable = true;

    [SerializeField] private GameObject clickParticleGO;
    [SerializeField] private Color clickColorWalk;
    [SerializeField] private Color clickColorRun;
    private ParticleSystem clickParticle;
    private ParticleSystem clickParticle2;

    #region Movement Variables
    private float walkSpeed = 3.44f;
    private float runSpeed = 6.4f;
    private float crouchSpeed = 1.71f;
    private float crouchRunSpeed = 2.74f;
    private float targetSpeed = 0.0f;
    private bool changingSpeed = false;
    private NavMeshAgent agent;
    [SerializeField] private float speedMultiplier = 1; // usar pra buff/debuff tbm, além da personagem ter um inicial diferente dependendo de classe... mas nunca pode ser %, tem q somar um valor temporariamente (item equipado ou habilidade temporaria)
    private RaycastHit clickHit;
    private Vector3 clickTarget;
    private Vector3 direction;
    private Quaternion lookRotation;
    private bool itemLookAt = false;
    private float lastClickTimestamp = 0;
    #endregion

    #region Animation Variables
    [SerializeField] private Animator animator;
    #endregion

    #region Noise Variables
    //public bool notSilent { get; private set; } = false;
    private float movementNoiseLevel = 0.0f;
    private float speedToNoiseRatio = 0.8f; // numero magico de conversão, mas pensando.. gadgets poderiam modificar isso, diminuindo o raio do noise. quanto menor, mais os inimigos ouvem
    private float nonMovementNoise = 0.0f;
    public float noiseLevel { get; private set; } = 0.0f; //numero já convertido para equivalente em distância
    #endregion

    #region BuffDebuff Variables
    private List<IBuffDebuff> buffDebuffModifiers;
    #endregion

    // Atributos provisórios de teste
    private bool changingColor = false;
    [SerializeField] Color initialColor;
    [SerializeField] Color spottedColor;
    [SerializeField] Color warningColor;
    private Color targetColor;
    private int colorFrameCounter = 25;
    [SerializeField] Material playerMaterial;
    [SerializeField] Material minimapMaterial;
    List<EnemyCharacter> inimigosVendo = new List<EnemyCharacter>();
    List<EnemyCharacter> inimigosOuvindo = new List<EnemyCharacter>();
    public Vector3 lastKnownLocation;
    public Vector3 lastHeardLocation;

    #region NerfGun Variables
    [Header("NerfGun")]
    [SerializeField] private GameObject NerfGGameObj;
    [SerializeField] public Transform nerfSpawnPoint;
    public NerfGun nerfGun;
    bool aimGunLook = false;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        _inventory = GetComponent<Inventory>();
    }
    private void OnEnable()
    {
        if (InventoryManager.GetInventoryManager() != null)
        {
            InventoryManager.GetInventoryManager().activePlayer = this;
            InventoryManager.GetInventoryManager().SetActiveInventory();
        }
    }
    private void Start()
    {
        buffDebuffModifiers = new List<IBuffDebuff>();
        ChangeColor(initialColor);
        agent = GetComponent<NavMeshAgent>();
        targetSpeed = walkSpeed;
        StealthDetectionManager.GetStealthDManager().RegisterPlayer(this);
        NerfGGameObj.SetActive(false);
        AkSoundEngine.RegisterGameObj(gameObject);
        //PlaySound("Play_Test1");//provisorio musica tocando

        clickParticleGO = GameObject.Instantiate(clickParticleGO);

        if (clickParticleGO)
        {
            ParticleSystem[] particles = clickParticleGO.GetComponentsInChildren<ParticleSystem>();
            clickParticle = particles[0];
            clickParticle2 = particles[1];
        }

        if (characterIndex == 0)
            FindObjectOfType<TutorialProvSave>().SetPlayerPos(gameObject); // provisorio
    }

    private void FixedUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    //SaveDataManager.GetSave().ForceDestroySaveData();
        //    SceneManager.LoadScene("MainMenu");
        //}
        CalculateNoiseLevel();

        //codigo mais ou menos provisório pro personagem olhar pro lugar certo quando para
        if (agent.velocity.sqrMagnitude <= 0.15f || aimGunLook || itemLookAt)
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 3);
        else
        {
            direction = (clickTarget - transform.position).normalized;
            lookRotation = Quaternion.LookRotation(direction);
        }

        animator.SetFloat("Speed", agent.velocity.magnitude);
        if (insideSafeZone)
            ChangeColor(initialColor);

#if UNITY_EDITOR
        // Visualizando o alcance do barulho
        Debug.DrawLine(transform.position, transform.position + Vector3.forward * noiseLevel);
        Debug.DrawLine(transform.position, transform.position + Vector3.right * noiseLevel);
        Debug.DrawLine(transform.position, transform.position + Vector3.left * noiseLevel);
        Debug.DrawLine(transform.position, transform.position + Vector3.back * noiseLevel);
        Debug.DrawLine(transform.position, transform.position + (Quaternion.Euler(0, 45, 0) * Vector3.forward) * noiseLevel);
        Debug.DrawLine(transform.position, transform.position + (Quaternion.Euler(0, 135, 0) * Vector3.forward) * noiseLevel);
        Debug.DrawLine(transform.position, transform.position + (Quaternion.Euler(0, 225, 0) * Vector3.forward) * noiseLevel);
        Debug.DrawLine(transform.position, transform.position + (Quaternion.Euler(0, 315, 0) * Vector3.forward) * noiseLevel);
#endif
    }
    #endregion

    #region Input
    public void Crouch(InputAction.CallbackContext context)
    {
        if (!context.started || !controllable)
            return;
        isCrouching = !isCrouching;
        if (isCrouching)
        {
            isRunning = false; //se abaixa no meio da corrida fica devagar, tem q clicar duas vzs d novo pra correr abaixado...? podemos tirar isso.
            animator.SetBool("Running", false);
            animator.SetBool("Crouching", true);
            // Animação de crouch provisória
            //transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y / 2, transform.localScale.z);
        }
        else
        {
            animator.SetBool("Crouching", false);
            // Animação em pé
            //transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * 2, transform.localScale.z);
        }
        GenerateNonMovementNoise(0.8f);
        RefreshMoveSpeed();
    }

    public void LeftClick(InputAction.CallbackContext context)
    {
        if (!context.performed || !controllable)
            return;
        if (Time.time - lastClickTimestamp >= 0.25f)
        {
            SingleClick();
        }
        else
        {
            DoubleClick();
        }
        lastClickTimestamp = Time.time;

        //if (!context.performed || doubleClicking || !controllable)
        //    return;
        //clickcount++;
        //print("clicks: " + clickcount);

        //if (RaycastClick(2080, 11))
        //{
        //    isRunning = false;
        //    animator.SetBool("Running", false);
        //    RefreshMoveSpeed();
        //    clickTarget = clickHit.point;
        //    agent.SetDestination(clickHit.point);
        //    clickParticle.Stop();
        //    clickParticleGO.transform.position = agent.destination + Vector3.up * 0.05f;
        //    clickParticle.startColor = clickColorWalk;
        //    clickParticle2.startColor = clickColorWalk;
        //    clickParticle.Play();
        //}
    }

    private void SingleClick()
    {
        if (MouseOverRaycaster.Instance.RaycastClick(2080, 11, ref clickHit))
        {
            isRunning = false;
            animator.SetBool("Running", false);
            RefreshMoveSpeed();
            clickTarget = clickHit.point;
            agent.SetDestination(clickHit.point);
            clickParticle.Clear();
            clickParticle2.Clear();
            clickParticleGO.transform.position = agent.destination + Vector3.up * 0.05f;
            clickParticle.startColor = clickColorWalk;
            clickParticle2.startColor = clickColorWalk;
            clickParticle.Play();
        }
    }

    private void DoubleClick()
    {
        if (MouseOverRaycaster.Instance.RaycastClick(2080, 11, ref clickHit))
        {
            isRunning = true;
            animator.SetBool("Running", true);
            RefreshMoveSpeed();
            agent.SetDestination(clickHit.point);
            //StopCoroutine(DoubleClickingDelay());
            //StartCoroutine(DoubleClickingDelay());
            clickParticle.Clear();
            clickParticle2.Clear();
            clickParticleGO.transform.position = agent.destination + Vector3.up * 0.05f;
            clickParticle.startColor = clickColorRun;
            clickParticle2.startColor = clickColorRun;
            clickParticle.Play();
        }
        clickTarget = clickHit.point;
    }

    //private bool RaycastClick(int layerMaskThatBlocks, int targetLayerMask)
    //{
    //    if (EventSystem.current.IsPointerOverGameObject())
    //        return false;
    //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //    if (!Physics.Raycast(ray, out clickHit, 200, layerMaskThatBlocks))
    //        return false;
    //    return clickHit.transform.gameObject.layer == targetLayerMask;
    //}

    //private IEnumerator DoubleClickingDelay()
    //{
    //    yield return new WaitForSecondsRealtime(0.3f);
    //    doubleClicking = false;
    //}

    public void RightClick(InputAction.CallbackContext context)
    {        
        if (!context.started || !controllable || CursorManager.GetCursorManager().currentState != CursorManager.CMStates.InGame) //TODO trocar pro GameStateManager
            return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200, 4128)) // 4128 = interactable + UI
        {
            switch (hit.transform.gameObject.layer)
            {
                case 12: // interactable
                    if (Vector3.Distance(transform.position, hit.transform.position) <= 1.4f && hit.transform.TryGetComponent(out IInteractable<ICharacter> obj))
                    {
                        LookAtItem(hit.transform);
                        if (obj.Interact(this))
                        {
                            if (hit.transform.position.y <= transform.position.y - 0.4f) //antes via se tava crouch, tem q fazer ambos e ter as anims grabgroundfromstand, grabhighfromground
                                animator.SetTrigger("PickItemLow");
                            else
                                animator.SetTrigger("PickItemHigh");
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void ResetCamera(InputAction.CallbackContext context)
    {
        if (!context.started)
            return;
        CameraManager.instance.ToggleFreeCamera();
    }

    #endregion

    #region Mudando de cor (vem do inimigo, muda a cor do minimap e dos detalhes do player)
    public void visibleBy(EnemyCharacter e)
    {
        //provisorio
        ChangeColor(spottedColor);
        seenByEnemy = true;
        if (!inimigosVendo.Contains(e))
        {
            inimigosVendo.Add(e);
        }
        lastKnownLocation = transform.position;
    }

    public void notVisibleBy(EnemyCharacter e)
    {
        //provisorio
        if (inimigosVendo.Contains(e))
            inimigosVendo.Remove(e);
        if (inimigosVendo.Count == 0)
        {
            if (inimigosOuvindo.Count == 0)
                ChangeColor(initialColor);
            else
                ChangeColor(warningColor);
            seenByEnemy = false;
        }
    }

    public void heardBy(EnemyCharacter e)
    {
        if (seenByEnemy)
            return;
        ChangeColor(warningColor);
        heardByEnemy = true;
        if (!inimigosOuvindo.Contains(e))
        {
            inimigosOuvindo.Add(e);
        }
        lastHeardLocation = transform.position;
    }

    public void notHeardBy(EnemyCharacter e)
    {
        if (inimigosOuvindo.Contains(e))
            inimigosOuvindo.Remove(e);
        if (inimigosOuvindo.Count == 0)
        {
            heardByEnemy = false;
            if (inimigosVendo.Count == 0)
                ChangeColor(initialColor);
        }
    }

    private void ChangeColor(Color _targetColor)
    {
        targetColor = _targetColor;
        if (!changingColor)
        {
            colorFrameCounter = 30;
            StartCoroutine(ColorChange());
        }
    }

    private IEnumerator ColorChange()
    {
        changingColor = true;
        while (colorFrameCounter > 0)
        {
            Color temp = Color.Lerp(playerMaterial.GetColor("_EmissionTint"), targetColor, 0.2f);
            playerMaterial.SetColor("_EmissionTint", temp);
            minimapMaterial.SetColor("_BaseColor", temp);
            colorFrameCounter--;
            yield return new WaitForFixedUpdate();
        }
        changingColor = false;
    }
    #endregion

    #region Eventos (Die, LoseTutorial, Win, forceCrouch, shootnerf)
    public void ShootNerfGun(Transform target)
    {
        direction = (target.position - transform.position).normalized;
        lookRotation = Quaternion.LookRotation(direction);
        aimGunLook = true;
        isCrouching = false;
        animator.SetTrigger("ShootNerf");
        agent.isStopped = true;
        controllable = false;
        CursorManager.GetCursorManager().LockUnavailable();
    }
    public void RevealNerfGun()
    {
        NerfGGameObj.SetActive(true);
    }
    public void NerfGunCallback()
    {
        nerfGun.GunFired();
        GenerateNonMovementNoise(26f);
        aimGunLook = false;
    }
    public void EndShootingSequence()
    {
        controllable = true;
        agent.isStopped = false;
        if (!nerfGun.wasStanding)
            CrouchForce(true);
        nerfGun.ForceEndUse();
        NerfGGameObj.SetActive(false);
    }
    public void CrouchForce(bool needsToCrouch)
    {
        isCrouching = needsToCrouch;
        if (isCrouching)
        {
            isRunning = false; //se abaixa no meio da corrida fica devagar, tem q clicar duas vzs d novo pra correr abaixado...? podemos tirar isso.

            animator.SetBool("Crouching", true);
        }
        GenerateNonMovementNoise(0.8f);
        RefreshMoveSpeed();
    }
    public void LoseTutorial()
    {
        if (controllable)
            animator.SetTrigger("SadWalk");
        controllable = false;
        //agent.isStopped = true;
        isRunning = false;
        isCrouching = true;
        TutorialProvSave temp = FindObjectOfType<TutorialProvSave>();
        agent.SetDestination(temp.initialSave.transform.position);
        RefreshMoveSpeed();
        StartCoroutine(ReloadScene(4.0f));
        temp.Die();
    }
    public void Die()
    {
        if (controllable)
            animator.SetTrigger("Death");
        Rigidbody temp = GetComponent<Rigidbody>();
        temp.constraints = RigidbodyConstraints.FreezeAll;
        agent.updateRotation = false;
        agent.updatePosition = false;
        controllable = false;
        //agent.stoppingDistance = 100f;
        agent.isStopped = true;
        StartCoroutine(ReloadScene(2.5f));
        FindObjectOfType<TutorialProvSave>().Die();
    }
    private IEnumerator ReloadScene(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private IEnumerator Winning()
    {
        yield return new WaitForSeconds(4.5f);
        SceneManager.LoadScene("FinalSurvey");
    }
    public void Win()
    {
        if (!InventoryManager.inventoryManager.SearchForItem(ItemType.Cookie)) return;
        controllable = false;
        //agent.stoppingDistance = 100f;
        //agent.isStopped = true;
        StartCoroutine(Winning());
        FindObjectOfType<TutorialProvSave>().Win();
    }


    #endregion

    #region Buffs e Debuffs

    public void AddBuff(IBuffDebuff origin)
    {
        switch (origin.GetBuffType())
        {
            case buffDebuffType.speed:
                if (!buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Add(origin);
                    speedMultiplier += origin.GetFloatModifier();
                }
                RefreshMoveSpeed();
                break;
            default:
                break;
        }
    }

    public void RemoveBuff(IBuffDebuff origin)
    {
        switch (origin.GetBuffType())
        {
            case buffDebuffType.speed:
                if (buffDebuffModifiers.Contains(origin))
                {
                    buffDebuffModifiers.Remove(origin);
                    speedMultiplier -= origin.GetFloatModifier();
                }
                RefreshMoveSpeed();
                break;
            default:
                break;
        }
    }

    #endregion

    #region Handling Movespeed and Speed-dependant Noise
    private void RefreshMoveSpeed()
    {
        switch (isRunning)
        {
            case true:
                if (isCrouching)
                    targetSpeed = crouchRunSpeed * speedMultiplier;
                else
                    targetSpeed = runSpeed * speedMultiplier;
                break;
            case false:
                if (isCrouching)
                    targetSpeed = crouchSpeed * speedMultiplier;
                else
                    targetSpeed = walkSpeed * speedMultiplier;
                break;
            default:
                break;
        }
        if (!changingSpeed)
            StartCoroutine(ChangingTargetSpeed());
    }

    private IEnumerator ChangingTargetSpeed() //
    {
        changingSpeed = true;
        WaitForFixedUpdate t = new WaitForFixedUpdate();
        while (changingSpeed)
        {
            float timeStep = 20 * Time.fixedDeltaTime;
            yield return t;
            agent.speed = Mathf.MoveTowards(agent.speed, targetSpeed, timeStep);
            movementNoiseLevel = Mathf.MoveTowards(movementNoiseLevel, targetSpeed * speedToNoiseRatio, timeStep);
            if (agent.speed == targetSpeed && movementNoiseLevel == targetSpeed * speedToNoiseRatio)
                changingSpeed = false;
        }
    }
    #endregion

    #region Handling Other Noise
    public void GenerateNonMovementNoise(float amount) // de forma que esses ruídos nunca se somam, só o mais alto prevalece
    {
        if (nonMovementNoise < amount)
            nonMovementNoise = amount;
    }

    private void CalculateNoiseLevel()
    {
        noiseLevel = Mathf.Clamp(movementNoiseLevel * agent.velocity.sqrMagnitude, 0, 14) + nonMovementNoise;
        AkSoundEngine.SetRTPCValue("MovementNoiseAmount", movementNoiseLevel * agent.velocity.sqrMagnitude);
        //print(movementNoiseLevel * agent.velocity.sqrMagnitude + "  move noise lvl   " + Time.frameCount);
        if (nonMovementNoise > 0) // non-movement noise decay
            nonMovementNoise = Mathf.MoveTowards(nonMovementNoise, 0, 0.02f + (nonMovementNoise * 4 * Time.fixedDeltaTime));
    }
    #endregion

    #region Items
    private void LookAtItem(Transform t)
    {
        direction = (t.position - transform.position).normalized;
        lookRotation = Quaternion.LookRotation(direction);
        itemLookAt = true;
        StartCoroutine(LookItem());
    }
    private IEnumerator LookItem()
    {
        yield return new WaitForSeconds(0.3f);
        itemLookAt = false;
    }
    #endregion

    #region SoundEngine
    public void PlaySound(string eventName)
    {
        AkSoundEngine.PostEvent(eventName, gameObject);
    }
    #endregion
}