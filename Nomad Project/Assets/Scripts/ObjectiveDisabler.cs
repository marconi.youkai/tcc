﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectiveDisabler : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [Range(0.1f, 20f)] public float timer;
    [Range(0.001f, 5f)] public float fadeInDuration;
    private bool fadedIn;
    [Range(0.001f, 5f)] public float fadeOutDuration;
    private bool fadedOut;
    [SerializeField] private bool autoDisable = false;
    private bool waitingForFadeOut = false;

    private void Start()
    {
        Reset();
        //if (SaveData.GetSave().firstSpawn)
        //    StartCoroutine(Disabler());
        //else
        text.gameObject.SetActive(false);
    }

    public void EnableObjective()
    {
        text.gameObject.SetActive(true);
        Reset();
        StartCoroutine(Enabler());
    }

    private void Reset()
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
        fadedIn = false;
        fadedOut = false;
        waitingForFadeOut = false;
    }

    private IEnumerator Enabler()
    {
        StartCoroutine(FadeIn());
        while (!fadedIn)
            yield return null;

        float t = 0;
        while (t < timer)
        {
            t = t + Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        waitingForFadeOut = true;
        if (autoDisable)
            StartCoroutine(FadeOut());

        while (!fadedOut)
            yield return null;

        text.gameObject.SetActive(false);
    }

    private IEnumerator FadeIn()
    {
        float t = 0;
        float i = 0;
        while (t <= timer)
        {
            i += Time.fixedDeltaTime / fadeInDuration;
            text.color = new Color(text.color.r, text.color.g, text.color.b, i);
            t += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        fadedIn = true;
    }

    private IEnumerator FadeOut()
    {
        while (!waitingForFadeOut)
            yield return null;

        float t = 0;
        float i = 1;
        while (t <= timer)
        {
            i -= Time.fixedDeltaTime / fadeInDuration;
            //print(i + "    alpha  " + Time.frameCount);
            text.color = new Color(text.color.r, text.color.g, text.color.b, i);
            t += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        fadedOut = true;
    }

    public void DisableObjective()
    {
        StartCoroutine(FadeOut());
    }
}
