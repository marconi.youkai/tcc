﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideMesh : MonoBehaviour
{
    [SerializeField] private bool isOn = true;
    
    private MeshRenderer[] probuilderMeshes;
    
    void Awake()
    {
        if (isOn)
        {
            probuilderMeshes = GetComponentsInChildren<MeshRenderer>();
            foreach (var mesh in probuilderMeshes)
            {
                mesh.enabled = false;
            }
        }
    }
}
