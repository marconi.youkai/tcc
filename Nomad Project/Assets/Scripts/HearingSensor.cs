using UnityEngine;

public class HearingSensor : MonoBehaviour, ISensor
{
    public bool Detected(EnemyCharacter e, PlayerCharacter p)
    {
        if (p == null)
            return false;
        return p.noiseLevel >= Vector3.Distance(e.transform.position, p.transform.position);
    }
}