﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTransitioner : MonoBehaviour
{
    [SerializeField] private AudioSource musicTutorial;
    [SerializeField] private AudioSource musicCave;
    [SerializeField] private float transitionDuration = 3;
    private bool alreadyPlayingCaveMusic = false;

    public void PlayTutorialMusic()
    {
        musicTutorial.Play();
    }

    public void PlayCaveMusic()
    {
        if (alreadyPlayingCaveMusic)
            return;
        alreadyPlayingCaveMusic = true;
        StartCoroutine(CrossFader(musicTutorial, musicCave, transitionDuration));
    }

    private IEnumerator CrossFader(AudioSource old, AudioSource next, float duration)
    {
        float counter = 0;
        float targetVolume = next.volume;
        float oldVolume = old.volume;
        next.volume = 0;
        next.Play();
        while (counter <= duration)
        {
            yield return new WaitForFixedUpdate();
            old.volume -= (oldVolume / duration) * Time.fixedDeltaTime;
            next.volume += (targetVolume / duration) * Time.fixedDeltaTime;
            counter += Time.fixedDeltaTime;
        }
        old.Stop();
    }
}
