﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NerfGun : InventoryItem
{
    InventoryManager invManager;
    PlayerCharacter owner;
    bool aiming = false;
    [SerializeField] private int startingAmmo = 3;
    [SerializeField] private float range = 12f;
    [SerializeField] private GameObject projectilePrefab;
    private Queue<NerfGunProjectile> projectiles = new Queue<NerfGunProjectile>();
    ShootableInteractable lastTarget;
    [HideInInspector] public bool wasStanding = false;
    bool alreadyShot = false;
    bool lockAimMode = false;
    bool authorizedToShoot = false;
    [SerializeField] private SpriteRenderer[] nerfs;
    [SerializeField] private GameObject nerfsMaster;

    private void OnEnable()
    {
        invManager = InventoryManager.GetInventoryManager();
        owner = invManager.activePlayer; //supondo q o onenable va acontecer toda vez q pega do chao e vai pro inv
        owner.nerfGun = this;
    }

    private void Start()
    {
        for (int i = 0; i < startingAmmo; i++)
        {
            NerfGunProjectile temp = Instantiate(projectilePrefab).GetComponent<NerfGunProjectile>();
            temp.owner = this;
            temp.InitializeNerf();
            projectiles.Enqueue(temp);
        }
        ReduceAmmo();
        //nerfsMaster.SetActive(false);
    }

    public override void Use()
    {
        if (lockAimMode)
            return;
        aiming = !aiming;
        if (aiming)
            EnterAimMode();
        else
            LeaveAimMode();
    }

    public void EnterAimMode()
    {
        //nerfsMaster.SetActive(true);
        StartCoroutine(Shoot());
        invManager.EnableCharInputAsset(false);
        CursorManager.GetCursorManager().StackState(CursorManager.CMStates.AimLock);
        //SoundEquipGun();
        //animator equipar arma
    }

    public void LeaveAimMode()
    {
        StopCoroutine(Shoot());
        invManager.EnableCharInputAsset();
        lockAimMode = false;
        CursorManager.GetCursorManager().LeaveState(CursorManager.CMStates.AimLock);
        //nerfsMaster.SetActive(false);
        //SoundUnequipGun();
        //animator guardar arma
    }

    private void TryShooting()
    {
        if (!authorizedToShoot)
            return;
        ShootableInteractable target = (ShootableInteractable) MouseOverRaycaster.Instance.currentMouseOverable;
        if (MouseOverRaycaster.Instance.currentMouseOverable != null)
        {
            alreadyShot = true;
            FireGun(target);
        }
    }

    public bool RaycastTarget(Vector3 targetPosition)
    {
        float dist = Vector3.Distance(owner.transform.position, targetPosition);
        if (dist <= range)
        {
            authorizedToShoot = !Physics.Raycast(owner.transform.position, targetPosition - owner.transform.position, dist, 3072); //3072 = layer 10 (2^10) + layer 11 (2^11) = highwall + floor
            return authorizedToShoot;
        }
        authorizedToShoot = false;
        return false;
    }

    private void FireGun(ShootableInteractable target)
    {
        //print("proj count " + projectiles.Count);
        if (projectiles.Count == 0)
        {
            owner.PlaySound("Play_NerfGun_Empty");
            ForceEndUse();
            return;
        }
        //raycast da ponta da arma para o target. se nao pegar em nenhuma wall, passa
        wasStanding = !owner.isCrouching;
        lockAimMode = true;
        owner.ShootNerfGun(target.transform);
        lastTarget = target;
    }

    public void GunFired()
    {
        owner.PlaySound("Play_NerfGun_Shot");
        NerfGunProjectile proj = projectiles.Dequeue();
        proj.gameObject.SetActive(true);
        proj.transform.position = invManager.activePlayer.nerfSpawnPoint.position; //temporário.. posicionar nerf em frente a ponta da arma
        proj.transform.LookAt(lastTarget.transform.position);
        proj.target = lastTarget;
        ReduceAmmo();
    }

    private void ReduceAmmo()
    {
        int i = nerfs.Length - projectiles.Count;
        if (i > 0)
            for (int j = 0; j < i; j++)
                nerfs[j].enabled = false;
    }

    public void ForceEndUse()
    {
        LeaveAimMode();
        aiming = false;
        alreadyShot = false;
    }

    public void ReceiveAmmo(NerfGunProjectile projectile)
    {
        projectile.owner = this;
        projectile.gameObject.SetActive(false);
        projectiles.Enqueue(projectile);
        if (projectiles.Count <= nerfs.Length)
        {
            nerfs[nerfs.Length - projectiles.Count].enabled = true;
        }
    }

    IEnumerator Shoot()
    {
        while (aiming && !alreadyShot)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))//TODO botar no input system novo
            {
                TryShooting();
            }
            yield return null;
        }

        yield return new WaitForEndOfFrame();
    }

    #region Sound

    private void SoundEquipGun() { }
    private void SoundFireGun() { }
    private void SoundNoAmmo() { }
    private void SoundUnequipGun() { }

    #endregion

    #region MouseOver Override
    public override CursorManager.CursorType MouseOverCursorType()
    {
        if (CursorManager.instance.currentState == CursorManager.CMStates.AimLock)
            return CursorManager.CursorType.Unavailable;
        return CursorManager.CursorType.MouseOver;
    }

    public override CursorManager.CMStates[] MouseOverStatesAllowed()
    {
        return new CursorManager.CMStates[2] { CursorManager.CMStates.InGame, CursorManager.CMStates.AimLock };
    }

    public override string MouseOverTooltip()
    {
        return "Click to enter or leave Aim Mode.\nShootable targets appear Green.\nGet your nerfs back!";
    }
    #endregion
}