﻿using UnityEngine;

public class NerfGunProjectile : MonoBehaviour, IInteractable<ICharacter>, IMouseOverable
{
    public NerfGun owner;
    public ShootableInteractable target;
    private Rigidbody rb;
    private CapsuleCollider capsCol;
    private bool alreadyHit = false;
    [SerializeField] private float speed = 10;
    [SerializeField] private float bouncebackMultiplier;

    private void Start()
    {
        AkSoundEngine.RegisterGameObj(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!alreadyHit && other.gameObject == target.gameObject)
        {
            target.TakeShot();
            alreadyHit = true;
            rb.useGravity = true;
            capsCol.enabled = true;
            rb.AddForce((-rb.transform.forward * 0.5f * bouncebackMultiplier) + (rb.transform.up * 1f * bouncebackMultiplier) + (rb.transform.right * Random.Range(-0.8f * bouncebackMultiplier, 0.8f * bouncebackMultiplier)), ForceMode.Impulse);
            // rb.AddTorque(new Vector3(0, Random.Range(-15f, 15f), Random.Range(-15f, 15f)), ForceMode.Impulse);//Random.Range(-0.6f, 0.6f)
            float temp = Random.Range(-1f, 1f);
            rb.AddForceAtPosition(rb.transform.right * temp, rb.transform.forward * 3f, ForceMode.Impulse);
            rb.AddForceAtPosition(rb.transform.right * -temp, -rb.transform.forward * 3f, ForceMode.Impulse);
            float temp2 = Random.Range(-1f, 1f);
            rb.AddForceAtPosition(rb.transform.up * temp2, rb.transform.forward * 3f, ForceMode.Impulse);
            rb.AddForceAtPosition(rb.transform.up * -temp2, -rb.transform.forward * 3f, ForceMode.Impulse);
            AkSoundEngine.PostEvent("Play_NerfGun_Hit", gameObject);
        }
    }

    public bool Interact(ICharacter condition)
    {
        RetrieveAmmo(false);
        return true;
    }

    public void RetrieveAmmo(bool mute)
    {
        if (!alreadyHit || InventoryManager.GetInventoryManager().activePlayer.nerfGun == null)
            return;
        if (!mute)
            AkSoundEngine.PostEvent("Play_NerfAmmo_Pickup", gameObject);
        rb.useGravity = false;
        capsCol.enabled = false;
        alreadyHit = false;
        rb.velocity = Vector3.zero;
        InventoryManager.GetInventoryManager().activePlayer.nerfGun.ReceiveAmmo(this);
    }

    private void FixedUpdate()
    {
        if (!alreadyHit)
        {
            transform.LookAt(target.transform.position);
            rb.MovePosition(transform.position + transform.forward * speed * Time.fixedDeltaTime);
        }
    }

    public void InitializeNerf()
    {
        rb = GetComponent<Rigidbody>();
        capsCol = GetComponent<CapsuleCollider>();
        alreadyHit = false;
        rb.useGravity = false;
        capsCol.enabled = false;
        gameObject.SetActive(false);
    }

    public string MouseOverTooltip()
    {
        return "Nerf ammo.\nBetter pick it up!";
    }

    public CursorManager.CMStates[] MouseOverStatesAllowed()
    {
        return new CursorManager.CMStates[1] { CursorManager.CMStates.InGame };
    }

    public CursorManager.CursorType MouseOverCursorType()
    {
        MouseOverRaycaster.Instance.StartContinuousRefresh();
        if (Vector3.Distance(InventoryManager.GetInventoryManager().activePlayer.transform.position, transform.position) <= 1.4f)
            return CursorManager.CursorType.Rightclick;
        else
            return CursorManager.CursorType.Unavailable;
    }
}
