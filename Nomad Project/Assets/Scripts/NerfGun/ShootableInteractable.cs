﻿using UnityEngine;
using UnityEngine.Events;

public class ShootableInteractable : MonoBehaviour, IMouseOverable, IInteractable<ICharacter>
{
    public UnityEvent linkedEvent;

    public void TakeShot()
    {
        linkedEvent.Invoke();
    }

    public CursorManager.CursorType MouseOverCursorType()
    {
        MouseOverRaycaster.Instance.StartContinuousRefresh();

        if (CursorManager.instance.currentState == CursorManager.CMStates.AimLock)
        {
            if (IsShootingTrajectoryUnblocked())
                return CursorManager.CursorType.MouseOver;
            else
                return CursorManager.CursorType.Unavailable;
        }


        if (Vector3.Distance(InventoryManager.GetInventoryManager().activePlayer.transform.position, transform.position) <= 1.4f)
            return CursorManager.CursorType.Rightclick;
        else
            return CursorManager.CursorType.Unavailable;
    }

    private bool IsShootingTrajectoryUnblocked()
    {
        return GameStateManager.Instance.playerCharacter.nerfGun.RaycastTarget(transform.position);
    }

    public CursorManager.CMStates[] MouseOverStatesAllowed()
    {
        return new CursorManager.CMStates[2] { CursorManager.CMStates.AimLock, CursorManager.CMStates.InGame };
    }

    public string MouseOverTooltip()
    {
        if (CursorManager.instance.currentState == CursorManager.CMStates.AimLock)
            return "If the cursor is RED, better position yourself.";
        else
            return "Right-Click to interact.\nYou need to be close!";
    }

    public bool Interact(ICharacter condition)
    {
        linkedEvent.Invoke();
        return true;
    }
}
