﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileRecoverySist : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.TryGetComponent(out NerfGunProjectile projectile))
        {
            projectile.RetrieveAmmo(true);
        }
    }
}
