﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSound : MonoBehaviour
{
    void Start()
    {
        gameObject.AddComponent<AkGameObj>();
        AkSoundEngine.RegisterGameObj(gameObject);
    }

    public void PlaySwitchSound()
    {
        AkSoundEngine.PostEvent("Play_lightswitch", gameObject);
    }
}
