﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MouseOverTooltip : MonoBehaviour
{
    private bool timerEnabled = false;
    private float timer = 0;
    [SerializeField] private float timerToDisplayTooltip = 3.0f;
    [SerializeField] private GameObject tooltipGO;
    private TextMeshProUGUI textbox;
    private bool displayingTooltip = false;
    private IMouseOverable lastObj;

    private void Start()
    {
        textbox = tooltipGO.GetComponentInChildren<TextMeshProUGUI>();
        tooltipGO.SetActive(false);
        MouseOverRaycaster.LostMouseOverTarget += LostMouseOver;
        MouseOverRaycaster.FoundMouseOverTarget += FoundMouseOver;
    }
    private void OnDisable()
    {
        MouseOverRaycaster.LostMouseOverTarget -= LostMouseOver;
        MouseOverRaycaster.FoundMouseOverTarget -= FoundMouseOver;
    }
    private void Update()
    {
        if (!timerEnabled)
            return;
        timer += Time.deltaTime;
        if (!displayingTooltip && timer >= timerToDisplayTooltip)
            DisplayTooltip();
        if (tooltipGO.activeSelf)
        {
            Vector3 targetPos = new Vector3(Mathf.Clamp(Input.mousePosition.x, 0.15f * Screen.width, 0.85f * Screen.width), Mathf.Clamp(Input.mousePosition.y, 0.15f * Screen.height, 0.88f * Screen.height), Input.mousePosition.z);
            tooltipGO.transform.position = Vector3.Lerp(tooltipGO.transform.position, targetPos, 0.1f);
        }

    }

    private void FoundMouseOver(IMouseOverable obj, bool refresh)
    {
        if(refresh)
            textbox.text = obj.MouseOverTooltip();
        if (timerEnabled)
            return;
        bool keepGoing = false;
        foreach (CursorManager.CMStates state in obj.MouseOverStatesAllowed())
        {
            if (state == CursorManager.GetCursorManager().currentState)
                keepGoing = true;
        }
        if (!keepGoing)
        {
            HideTooltip();
            return;
        }
        if (obj != lastObj)
        {
            timer = 0;
            lastObj = obj;
        }
        timerEnabled = true;
        textbox.text = obj.MouseOverTooltip();
    }

    private void LostMouseOver()
    {
        HideTooltip();
    }

    private void DisplayTooltip()
    {
        tooltipGO.transform.position = Input.mousePosition;
        tooltipGO.SetActive(true);
        displayingTooltip = true;
    }

    private void HideTooltip()
    {
        timerEnabled = false;
        displayingTooltip = false;
        tooltipGO.SetActive(false);
    }
}
