﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    #region Singleton (Awake + OnDestroy)
    public static CursorManager instance;
    public static CursorManager GetCursorManager() { return instance; }
    private void Awake() { if (instance == null) instance = this; else Destroy(this); }
    private void OnDestroy()
    {
        if (instance == this) instance = null;
        currentState = CMStates.General;
        SetCursor(CMStates.General);
    }
    #endregion

    #region Variables
    public enum CursorType { Default, Unavailable, Rightclick, MouseOver };
    private Dictionary<CMStates, Texture2D> defaultHashtable;
    private Dictionary<CMStates, Texture2D> mouseOverHashtable;
    private Dictionary<CMStates, Texture2D> rightclickHashtable;
    private Dictionary<CMStates, Texture2D> unavailableHashtable;
    private Dictionary<CMStates, Vector2> hotspotHashtable;
    private bool locked = false;
    [Header("Cursor Texture2Ds")]
    [Space(20)]
    [SerializeField] public Texture2D defaultEmpty;
    [SerializeField] public Texture2D mouseOverEmpty;
    [SerializeField] public Texture2D rightclickEmpty;
    [SerializeField] public Texture2D unavailableEmpty;
    [SerializeField] public Vector2 emptyHotspot;
    [Space(10)]
    [SerializeField] public Texture2D defaultPaused;
    [SerializeField] public Texture2D mouseOverPaused;
    [SerializeField] public Texture2D rightclickPaused;
    [SerializeField] public Texture2D unavailablePaused;
    [SerializeField] public Vector2 pausedHotspot;
    [Space(10)]
    [SerializeField] public Texture2D defaultInGame;
    [SerializeField] public Texture2D mouseOverInGame;
    [SerializeField] public Texture2D rightclickInGame;
    [SerializeField] public Texture2D unavailableInGame;
    [SerializeField] public Vector2 ingameHotspot;
    [Space(10)]
    [SerializeField] public Texture2D defaultDialog;
    [SerializeField] public Texture2D mouseOverDialog;
    [SerializeField] public Texture2D rightclickDialog;
    [SerializeField] public Texture2D unavailableDialog;
    [SerializeField] public Vector2 dialogHotspot;
    [Space(10)]
    [SerializeField] public Texture2D defaultAim;
    [SerializeField] public Texture2D mouseOverAim;
    [SerializeField] public Texture2D rightclickAim;
    [SerializeField] public Texture2D unavailableAim;
    [SerializeField] public Vector2 aimHotspot;
    #endregion

    #region "StateMachine"
    #region Variables StateMachine
    public enum CMStates { General, GamePaused, InGame, DialogLock, AimLock }
    public CMStates currentState { get; private set; } = CMStates.General;
    public CursorType currentCursor { get; private set; }
    private List<CMStates> stateStack;
    #endregion
    #region Methods StateMachine
    public void StackState(CMStates targetState)
    {
        stateStack.Add(currentState);
        currentState = targetState;
        SetCursor(currentState);
        MouseOverRaycaster.Instance.RefreshMouseOverTargetInfo();
    }
    public void LeaveState(CMStates stateToLeave)
    {
        if (currentState == stateToLeave)
        {
            if (stateStack.Count > 0)
            {
                int i = stateStack.Count - 1;
                currentState = stateStack[i];
                stateStack.RemoveAt(i);
            }
            else
                currentState = CMStates.General;
            UnlockUnavailable();
            MouseOverRaycaster.Instance.RefreshMouseOverTargetInfo();
        }
        else if (stateStack.Contains(stateToLeave))
            stateStack.Remove(stateToLeave);
    }

    #endregion
    #endregion

    #region Initialization
    private void InitializeStateMachine()
    {
        stateStack = new List<CMStates>();
    }

    private void InitializeCursorHastables()
    {
        defaultHashtable = new Dictionary<CMStates, Texture2D>();
        mouseOverHashtable = new Dictionary<CMStates, Texture2D>();
        rightclickHashtable = new Dictionary<CMStates, Texture2D>();
        unavailableHashtable = new Dictionary<CMStates, Texture2D>();
        hotspotHashtable = new Dictionary<CMStates, Vector2>();

        defaultHashtable.Add(CMStates.General, defaultEmpty);
        mouseOverHashtable.Add(CMStates.General, mouseOverEmpty);
        rightclickHashtable.Add(CMStates.General, rightclickAim);
        unavailableHashtable.Add(CMStates.General, unavailableEmpty);
        hotspotHashtable.Add(CMStates.General, emptyHotspot);

        defaultHashtable.Add(CMStates.AimLock, defaultAim);
        mouseOverHashtable.Add(CMStates.AimLock, mouseOverAim);
        rightclickHashtable.Add(CMStates.AimLock, rightclickAim);
        unavailableHashtable.Add(CMStates.AimLock, unavailableAim);
        hotspotHashtable.Add(CMStates.AimLock, aimHotspot);

        defaultHashtable.Add(CMStates.DialogLock, defaultDialog);
        mouseOverHashtable.Add(CMStates.DialogLock, mouseOverDialog);
        rightclickHashtable.Add(CMStates.DialogLock, rightclickDialog);
        unavailableHashtable.Add(CMStates.DialogLock, unavailableDialog);
        hotspotHashtable.Add(CMStates.DialogLock, dialogHotspot);

        defaultHashtable.Add(CMStates.GamePaused, defaultPaused);
        mouseOverHashtable.Add(CMStates.GamePaused, mouseOverPaused);
        rightclickHashtable.Add(CMStates.GamePaused, rightclickPaused);
        unavailableHashtable.Add(CMStates.GamePaused, unavailablePaused);
        hotspotHashtable.Add(CMStates.GamePaused, pausedHotspot);

        defaultHashtable.Add(CMStates.InGame, defaultInGame);
        mouseOverHashtable.Add(CMStates.InGame, mouseOverInGame);
        rightclickHashtable.Add(CMStates.InGame, rightclickInGame);
        unavailableHashtable.Add(CMStates.InGame, unavailableInGame);
        hotspotHashtable.Add(CMStates.InGame, ingameHotspot);
    }
    #endregion

    #region Unity Methods
    private void Start()
    {
        InitializeStateMachine();

        InitializeCursorHastables();

        StackState(CMStates.InGame);

        MouseOverRaycaster.FoundMouseOverTarget += FoundMouseOver;
        MouseOverRaycaster.ContinuousRefreshMouseOverTarget += FoundMouseOver;
        MouseOverRaycaster.LostMouseOverTarget += LostMouseOver;
    }
    //private void Update()
    //{
    //    print(currentState + "  " + Time.frameCount);
    //    print(locked + " -> lock  " + Time.frameCount);
    //}
    private void OnDisable()
    {
        MouseOverRaycaster.FoundMouseOverTarget -= FoundMouseOver;
        MouseOverRaycaster.ContinuousRefreshMouseOverTarget -= FoundMouseOver;
        MouseOverRaycaster.LostMouseOverTarget -= LostMouseOver;
    }
    #endregion

    #region External Calls
    private void FoundMouseOver(IMouseOverable obj, bool refresh)
    {
        if (obj == null)
            return;
        bool keepGoing = false;
        foreach (CMStates state in obj.MouseOverStatesAllowed())
        {
            if (state == currentState)
                keepGoing = true;
        }
        if (!keepGoing)
            return;
        SetCursor(currentState, obj.MouseOverCursorType());
    }

    private void LostMouseOver()
    {
        SetCursor(currentState);
    }

    /// <summary>
    /// Deprecated
    /// </summary>
    /// <param name="callerType"></param>
    /// <param name="image"></param>
    public void SetCursor(CMStates callerType, Texture2D image)
    {
        if (currentState != callerType || locked)
            return;
        Cursor.SetCursor(image, hotspotHashtable[currentState], CursorMode.ForceSoftware);
    }

    /// <summary>
    /// Deprecated
    /// </summary>
    /// <param name="callerType"></param>
    /// <param name="image"></param>
    /// <param name="hotspot"></param>
    public void SetCursor(CMStates callerType, Texture2D image, Vector2 hotspot)
    {
        if (currentState != callerType || locked)
            return;
        Cursor.SetCursor(image, hotspot, CursorMode.ForceSoftware);
    }

    public void SetCursor(CMStates callerType, CursorType cursor = CursorType.Default)
    {
        if (currentState != callerType || locked)
            return;
        switch (cursor)
        {
            case CursorType.Default:
                Cursor.SetCursor(defaultHashtable[currentState], hotspotHashtable[currentState], CursorMode.ForceSoftware);
                currentCursor = CursorType.Default;
                break;
            case CursorType.MouseOver:
                Cursor.SetCursor(mouseOverHashtable[currentState], hotspotHashtable[currentState], CursorMode.ForceSoftware);
                currentCursor = CursorType.MouseOver;
                break;
            case CursorType.Rightclick:
                Cursor.SetCursor(rightclickHashtable[currentState], hotspotHashtable[currentState], CursorMode.ForceSoftware);
                currentCursor = CursorType.Rightclick;
                break;
            case CursorType.Unavailable:
                Cursor.SetCursor(unavailableHashtable[currentState], hotspotHashtable[currentState], CursorMode.ForceSoftware);
                currentCursor = CursorType.Unavailable;
                break;
            default:
                break;
        }
    }

    public void LockUnavailable()
    {
        SetCursor(currentState, CursorType.Unavailable);
        locked = true;
    }

    public void UnlockUnavailable()
    {
        locked = false;
        SetCursor(currentState, CursorType.Default);
    }
    #endregion


}
