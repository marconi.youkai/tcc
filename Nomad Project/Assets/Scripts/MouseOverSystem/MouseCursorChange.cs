﻿using UnityEngine;

public class MouseCursorChange : MonoBehaviour
{
    public CursorManager.CMStates cursorType;
    private CursorManager cManager;
    public Texture2D cursorNormal;
    public Texture2D cursorMouseOver;
    public Texture2D cursorUnavailable;
    private InventoryManager invManager;
    public Vector2 hotspot = Vector2.zero;
    [SerializeField] private bool hasToStand = false;
    [SerializeField] private bool minDistanceCheck = false;
    [SerializeField] private float minDistance = 1.4f;

    void Start()
    {
        //Cursor.SetCursor(cursorNormal, Vector2.zero, CursorMode.ForceSoftware);
        invManager = InventoryManager.GetInventoryManager();
        cManager = CursorManager.GetCursorManager();
    }

    private void OnMouseOver()
    {
        if (minDistanceCheck && Vector3.Distance(invManager.activePlayer.transform.position, transform.position) > minDistance)
            cManager.SetCursor(cursorType, cursorUnavailable, hotspot);
        else if (hasToStand && invManager.activePlayer.isCrouching)
            cManager.SetCursor(cursorType, cursorUnavailable, hotspot);
        else
            cManager.SetCursor(cursorType, cursorMouseOver, hotspot);
    }
    private void OnMouseExit()
    {
        ResetCursor();
    }

    private void ResetCursor()
    {
        if (cManager != null)
            cManager.SetCursor(cursorType, cursorNormal, hotspot);
    }

    private void OnDestroy() { ResetCursor(); }
}
