﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseOverRaycaster : MonoBehaviour
{
    public static MouseOverRaycaster Instance { get; private set; }

    public static event Action LostMouseOverTarget;
    public static event Action<IMouseOverable, bool> FoundMouseOverTarget;
    public static event Action<IMouseOverable, bool> ContinuousRefreshMouseOverTarget; //TODO o alvo vai ser o cursor manager, mas quem habilita é o mouseOverable. pode ser interrompido aqui no mouseexit ou pelo próprio moseoverable?

    [SerializeField] private LayerMask _layerMask;
    public IMouseOverable currentMouseOverable { get; private set; }
    private RaycastHit[] _hit;
    private Camera _mainCam;
    Coroutine _continuousRefresh = null;

    public bool RaycastClick(int layerMaskThatBlocks, int targetLayerMask, ref RaycastHit clickHit)
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return false;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(ray, out clickHit, 200, layerMaskThatBlocks))
            return false;
        return clickHit.transform.gameObject.layer == targetLayerMask;
    }

    public void RefreshMouseOverTargetInfo()
    {
        if (currentMouseOverable == null)
            return;
        FoundMouseOverTarget?.Invoke(currentMouseOverable, true);
    }

    public void StartContinuousRefresh()
    {
        if (_continuousRefresh == null)
            _continuousRefresh = StartCoroutine(ContinuousRefresh());
    }

    public void EndContinuousRefresh()
    {
        if (_continuousRefresh != null)
        {
            StopCoroutine(_continuousRefresh);
            _continuousRefresh = null;
        }
    }
    private IEnumerator ContinuousRefresh()
    {
        while (_continuousRefresh == null)
        {
            yield return null;
        }
        while (true)
        {
            if (ContinuousRefreshMouseOverTarget != null && currentMouseOverable != null)
                ContinuousRefreshMouseOverTarget(currentMouseOverable, false);
            yield return new WaitForSeconds(0.031f);
        }
    }

    #region UnityMethods
    private void Awake()
    {
        if (Instance == null) Instance = this; else Destroy(this);
    }

    private void OnDestroy()
    {
        if (Instance == this) Instance = null;
    }

    private void Start()
    {
        _mainCam = Camera.main;
    }

    private void Update()
    {
        Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);
        _hit = Physics.RaycastAll(ray, 50, _layerMask);
        if (_hit.Length == 0)
        {
            MouseOverIsNull();
            return;
        }
        float lastDot = -1.1f;
        IMouseOverable targetMouseOverable = null;
        for (int i = 0; i < _hit.Length; i++)
        {
            if (!_hit[i].transform.gameObject.TryGetComponent(out IMouseOverable mouseOverable))
                continue;
            float newDot = Vector3.Dot(ray.direction, (_hit[i].transform.position - _mainCam.transform.position).normalized);
            if (newDot > lastDot)
            {
                lastDot = newDot;
                targetMouseOverable = mouseOverable;
            }
        }
        if (targetMouseOverable == null)
        {
            MouseOverIsNull();
            return;
        }
        if (targetMouseOverable != currentMouseOverable)
        {
            currentMouseOverable = targetMouseOverable;
            EndContinuousRefresh();
            LostMouseOverTarget?.Invoke();
            FoundMouseOverTarget?.Invoke(currentMouseOverable, false);
        }
    }
    #endregion

    private void MouseOverIsNull()
    {
        if (currentMouseOverable != null)
        {
            EndContinuousRefresh();
            currentMouseOverable = null;
            LostMouseOverTarget?.Invoke();
        }
    }
}
