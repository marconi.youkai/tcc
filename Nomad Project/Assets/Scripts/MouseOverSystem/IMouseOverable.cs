﻿

public interface IMouseOverable
{
    string MouseOverTooltip();
    CursorManager.CMStates[] MouseOverStatesAllowed();
    CursorManager.CursorType MouseOverCursorType();
}
