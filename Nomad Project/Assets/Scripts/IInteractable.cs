﻿public interface IInteractable<T>
{
    bool Interact(T condition);
}
