using UnityEngine;

public enum ItemType
{
    Null,
    Cookie,
    SilentShoes,
    NerfGun,
    NerfProjectile
}

public struct Item
{
    public Item(ItemType _type, string _description, int _marketValue, int _level, GameObject _inventoryIcon, GameObject _inGameModel, bool _obtainable, float _noiseOnInteraction)
    {
        type = _type;
        description = _description;
        marketValue = _marketValue;
        level = _level;
        InventoryIconPrefab = _inventoryIcon;
        inGamePrefab = _inGameModel;
        obtainable = _obtainable;
        noiseOnInteraction = _noiseOnInteraction;
    }

    public Item(ItemType _type = ItemType.Null)
    {
        type = _type;
        description = "";
        marketValue = 0;
        level = 0;
        InventoryIconPrefab = null;
        inGamePrefab = null;
        obtainable = false;
        noiseOnInteraction = 0;
    }

    //
    public ItemType type { get; private set; }
    public string description { get; private set; }
    public int marketValue { get; private set; }
    public bool obtainable { get; private set; }
    public float noiseOnInteraction { get; private set; }
    public int level { get; private set; } // a ideia desse numero seria um multiplicador no efeito/cooldown da habilidade do item. pode influenciar diretamente o marketvalue. poderia ser modificado externamente se tiver algum esquema de melhorar itens
    public GameObject InventoryIconPrefab;// { get; private set; }
    public GameObject inGamePrefab;// { get; private set; } //se jogar o item no chao
}