﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowLightRegion : MonoBehaviour, IBuffDebuff
{
    [Header("Se a luz começa apagada, selecionar aqui:")]
    [SerializeField] private bool lightsOff = false;
    private List<EnemyCharacter> enemiesInside;

    private void Start()
    {
        enemiesInside = new List<EnemyCharacter>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out EnemyCharacter e))
        {
            enemiesInside.Add(e);
            if (lightsOff)
                e.AddBuff(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out EnemyCharacter e))
        {
            enemiesInside.Remove(e);
            e.RemoveBuff(this);
        }
    }

    public void Switch()
    {
        lightsOff = !lightsOff;
        if (lightsOff)
        {
            foreach (EnemyCharacter e in enemiesInside)
                e.AddBuff(this);
        }
        else
        {
            foreach (EnemyCharacter e in enemiesInside)
                e.RemoveBuff(this);
        }
    }

    public buffDebuffType GetBuffType()
    {
        return buffDebuffType.visionAndInverseFoV;
    }

    public float GetFloatModifier()
    {
        return -0.55f;
    }
}
