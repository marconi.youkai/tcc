using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private InventorySlot[] _inventorySlots;
    public InventorySlot[] _InventorySlots => _inventorySlots;
    private List<int> emptySlots;
    public bool invetoryFull => emptySlots.Count <= 0;
    public int nextEmptySlot => emptySlots[0];


    public Inventory(int slotAmount)
    {
        _inventorySlots = new InventorySlot[slotAmount];
        initializeEmptySlotList(_inventorySlots.Length);
    }

    public Inventory(InventorySlot[] inventorySlots)
    {
        _inventorySlots = inventorySlots;
        initializeEmptySlotList(_inventorySlots.Length);
    }

    public void BindSlotsAnchors(InventorySlot[] inventorySlots)
    {
        _inventorySlots = inventorySlots;
        initializeEmptySlotList(_inventorySlots.Length);
    }

    private void initializeEmptySlotList(int slotAmount)
    {
        emptySlots = new List<int>();
        for (int i = 0; i < _inventorySlots.Length; i++)
        {
            emptySlots.Add(i);
        }
    }

    /// <summary>
    /// Insert an item into next available empty slot
    /// </summary>
    /// <param name="item">Item to be inserted</param>
    public void InsertItem(InventoryItem item)
    {
        if (invetoryFull)
        {
#if UNITY_EDITOR
            Debug.LogWarning("Inventory is full");
#endif
            return;
        }

        InventorySlot slot = _inventorySlots[nextEmptySlot];
        
        if (!slot.empty)
            return;
        slot.AddItem(item);
        emptySlots.Remove(nextEmptySlot);
    }

    /// <summary>
    /// Insert an item into specified slot if empty
    /// </summary>
    /// <param name="item">Item to be inserted</param>
    public void InsertItem(int slotIndex, InventoryItem item)
    {
        if (invetoryFull)
        {
#if UNITY_EDITOR
            Debug.LogWarning("Inventory is full");
#endif
            return;
        }

        if (slotIndex < 0 || slotIndex >= _inventorySlots.Length)
        {
#if UNITY_EDITOR
            Debug.LogError("Invalid inventory slot: " + slotIndex);
#endif
            return;
        }
        
        InventorySlot slot = _inventorySlots[slotIndex];
        
        if (!slot.empty)
            return;

        slot.AddItem(item);
        emptySlots.Remove(slotIndex);
    }

    public void InsertItem(InventorySlot slot, InventoryItem item)
    {
        InsertItem(GetSlotIndex(slot), item);
    }

    public void RemoveItem(int slotIndex)
    {
        if (emptySlots.Contains(slotIndex))
            return;
        _inventorySlots[slotIndex].RemoveItem();
        emptySlots.Add(slotIndex);
        emptySlots.Sort();
    }

    public void RemoveItem(InventorySlot slot)
    {
       RemoveItem(GetSlotIndex(slot));
    }

    public InventoryItem GetItem(int slotIndex)
    {
        return _inventorySlots[slotIndex].empty ? null : _inventorySlots[slotIndex].invItem;
    }

    public InventoryItem GetItem(InventorySlot slot)
    {
        return slot.invItem;
    }

    public InventorySlot GetSlot(int index)
    {
        return _inventorySlots[index];
    }

    public int GetSlotIndex(InventorySlot slot)
    {
        for (int i = 0; i < _inventorySlots.Length; i++)
        {
            if (_inventorySlots[i] == slot)
                return i;
        }

        return 0;
    }

    public void UseItem(int slotIndex)
    {
        _inventorySlots[slotIndex].invItem?.Use();
    }
}