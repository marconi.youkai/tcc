﻿using UnityEngine;

public class InventorySlot : MonoBehaviour
{
    public InventoryItem invItem;
    private Transform _itemTrans;
    public bool empty = true;
    public Transform anchor; // { get; private set; }

    public void AddItem(InventoryItem receivedItem)
    {
        if (empty)
        {
            invItem = receivedItem;

            _itemTrans = invItem.transform;
            _itemTrans.SetParent(anchor);
            _itemTrans.localPosition = Vector3.zero;
            _itemTrans.localScale = Vector3.one;
            _itemTrans.localRotation = Quaternion.identity;
            empty = false;
        }
        else
        {
            _itemTrans = invItem.transform;
            _itemTrans.localPosition = Vector3.zero;
            invItem = receivedItem;
            _itemTrans.SetParent(anchor);
        }
    }

    public void RemoveItem()
    {
        invItem = null;
        _itemTrans = null;
        empty = true;
    }
}