﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : ScriptableObject
{
    public static SaveData Instance;// { get; set; }// = null;
    public Vector3 playerPosition;// { get; set; }// = null;
    public Quaternion playerRotation;// { get; set; }// = null;
    public bool playerIsCrouched;// { get; set; }// = false;
    public bool hasNerfgun;// { get; set; }// = false;
    public bool firstSpawn = true;// { get; set; }// = true;
    public bool hasCookie;// { get; set; }// = false;

    //private bool booleanaPrivada = true;
    

    //public Tuple<int, bool> testandoTuple;

    //public int testInt = 0;
    //public int[] intArray;
    //public List<PlayerCharacter> characterList;


    //public Dictionary<EnemyCharacter, Vector3> enemyPositionDictionary;

    //private Vector3 testev3;
    //public Vector3 Testev3 { get => testev3; set => testev3 = value; }



    //public Vector3 playerPosition { get; private set; }
    //public Vector3 playerPosition { get; private set; }
    //public Vector3 playerPosition { get; private set; }
    //public Vector3 playerPosition { get; private set; }
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    //public void LoadSaveData(SaveData loadedData)
    //{
    //    if (Instance != null)
    //        Destroy(Instance);
    //    Instance = this;
    //    playerPosition = loadedData.playerPosition;
    //    playerIsCrouched = loadedData.playerIsCrouched;
    //}
}