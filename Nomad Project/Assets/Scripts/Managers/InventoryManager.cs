using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using CameraSystem;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager inventoryManager;

    public static InventoryManager GetInventoryManager()
    {
        return inventoryManager;
    } // singleton

    [SerializeField] private LayerMask inventoryLayerMask = 16384;
    [SerializeField] private InventorySlot[] slotAnchors;
    [SerializeField] private GameObject backpack;
    [SerializeField] private InputActionAsset[] characterInputMaps;
    private Inventory _activeInventory;
    public Inventory Inventory => _activeInventory;
    private int LastSlotClicked = 0;

    // private List<InventorySlot> char1Inventory;

    private MinimapCam minimapCam;

    public PlayerCharacter activePlayer;

    private RaycastHit clickHit;


    #region Drag Variables

    private Transform draggedTransform;
    [SerializeField] private Transform hitplaneOrigin;
    private Plane hitplane;
    private bool dragging = false;
    private InventoryItem draggedItem;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (inventoryManager == null)
            inventoryManager = this;
        else
            Destroy(this);
        if (activePlayer == null)
        {
            if (FindObjectOfType<PlayerCharacter>() != null)
            {
                activePlayer = FindObjectOfType<PlayerCharacter>();

                if (activePlayer._Inventory == null)
                    activePlayer.SetInventory(activePlayer.GetComponent<Inventory>());
                SetActiveInventory();
            }
        }
        else
        {
            if (activePlayer._Inventory == null)
                activePlayer.SetInventory(activePlayer.GetComponent<Inventory>());
            SetActiveInventory();
        }
    }

    private void Start()
    {
        hitplane = new Plane(Vector3.up, 0);
        characterInputMaps[1].Disable();
        minimapCam = FindObjectOfType<MinimapCam>();

    }

    public void SetActiveInventory()
    {
        _activeInventory = activePlayer._Inventory;
        _activeInventory.BindSlotsAnchors(slotAnchors);
    }

    private void FixedUpdate()
    {
        if (!dragging || draggedTransform == null)
            return;

        hitplane.SetNormalAndPosition(Camera.main.transform.forward, hitplaneOrigin.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance;
        if (hitplane.Raycast(ray, out distance))
            draggedTransform.position = Vector3.Lerp(draggedTransform.position, ray.GetPoint(distance), 10 * Time.fixedDeltaTime);
    }

    private void OnDestroy()
    {
        if (inventoryManager == this)
            inventoryManager = null;
    }
    private void OnDisable()
    {
        if (inventoryManager == this)
            inventoryManager = null;
    }

    #endregion

    // should get its own class to handle input

    #region Player Input

    public void Drag(InputAction.CallbackContext context)
    {
        InventorySlot clickedSlot;
        switch (context.phase)
        {
            case InputActionPhase.Performed:
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 50, inventoryLayerMask) && hit.transform.TryGetComponent<InventorySlot>(out clickedSlot))
                {
                    draggedItem = _activeInventory.GetItem(clickedSlot);
                    if (draggedItem == null)
                        break;
                    LastSlotClicked = _activeInventory.GetSlotIndex(clickedSlot);
                    _activeInventory.RemoveItem(LastSlotClicked);
                    draggedTransform = draggedItem.transform;
                    dragging = true;
                }

                break;
            case InputActionPhase.Canceled:
                if (dragging)
                {
                    dragging = false;
                    DragReleaseHandler();
                }

                break;
            default:
                break;
        }
    }

    public void UseItem(InputAction.CallbackContext context)
    {
        if (!context.performed)
            return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (!Physics.Raycast(ray, out var hit, 50.0f, inventoryLayerMask))
            return;


        if (hit.collider.TryGetComponent<InventorySlot>(out var slot))
        {
            if (slot.invItem != null)
                slot.invItem.Use();
            return;
        }
    }

    public void UseItem1Slot()
    {
        _activeInventory.UseItem(0);
    }
    public void UseItem2Slot()
    {
        _activeInventory.UseItem(1);
    }
    public void UseItem3Slot()
    {
        _activeInventory.UseItem(2);
    }
    public void UseItem4Slot()
    {
        _activeInventory.UseItem(3);
    }

    public void BackpackToggle()
    {
        backpack.SetActive(!backpack.activeSelf);
    }

    public void ScrollZoom(InputAction.CallbackContext context)
    {
        if (!context.started)
            return;
        CameraManager.instance.CameraZoom((int)Mathf.Sign(context.ReadValue<float>()));
    }

    public void ClickPlayer(InputAction.CallbackContext context)
    {
        if (!context.performed)
            return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (!Physics.Raycast(ray, out hit, 100, 256))
            return;

        if (!hit.transform.TryGetComponent(out PlayerCharacter p))
            return;

        //CameraManager.instance.ChangeCharCam(p.gameObject); // removi temporariamente pq me irritou tentar andar perto e centralizar a camera! posicionei estrategicamente e clico sem querer
        activePlayer = p;
        minimapCam.UpdatePlayer(p.transform);

        //EnableCharInputAsset();
    }

    public void EnableCharInputAsset(bool enable = true)
    {
        if (enable)
        {
            switch (activePlayer.characterIndex) // aqui habilita e desabilita o inputactionasset do personagem
            {
                case 0:
                    characterInputMaps[0].Enable();
                    characterInputMaps[1].Disable();
                    break;
                case 1:
                    characterInputMaps[1].Enable();
                    characterInputMaps[0].Disable();
                    break;
                default:
                    break;
            }
        }
        else
        {
            for (int i = 0; i < characterInputMaps.Length; i++)
            {
                characterInputMaps[i].Disable();
            }
        }
    }

    #endregion

    #region Snap & Swap

    private void DragReleaseHandler()
    {
        if (draggedTransform == null)
            return;
        if (RaycastClick() && clickHit.collider.TryGetComponent(out InventorySlot slot))
        {
            if (!slot.empty)
            {
                Swap(slot, slot.invItem, _activeInventory.GetSlot(LastSlotClicked), draggedItem);
            }

            int slotIndex = _activeInventory.GetSlotIndex(slot);
            _activeInventory.InsertItem(slotIndex, draggedItem);
        }
        else
            _activeInventory.InsertItem(LastSlotClicked, draggedItem);

        draggedTransform.localPosition = Vector3.zero;
    }

    private void Swap(InventorySlot inventorySlot1, InventoryItem inventoryItem1, InventorySlot inventorySlot2, InventoryItem inventoryItem2)
    {
        var item1 = inventoryItem1;
        var item2 = inventoryItem2;
        _activeInventory.RemoveItem(inventorySlot1);
        _activeInventory.InsertItem(inventorySlot1, inventoryItem2);
        _activeInventory.RemoveItem(inventorySlot2);
        _activeInventory.InsertItem(inventorySlot2, inventoryItem1);
    }

    #endregion

    private bool RaycastClick()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return false;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out clickHit, 200, inventoryLayerMask); // 32 = UI
    }

    public bool SearchForItem(ItemType item)
    {
        foreach (var inventorySlot in _activeInventory._InventorySlots)
        {
            if (inventorySlot.invItem == null)
                return false;
            if (inventorySlot.invItem.Item.type == item)
                return true;
        }

        return false;
    }
}