using System.Collections.Generic;
using UnityEngine;

public class ViewConeRenderer : MonoBehaviour
{

    private Mesh meshShort;
    private Mesh meshLong;
    private Vector3 origin;

    [SerializeField] private MeshFilter meshFilterShort;
    [SerializeField] private MeshFilter meshFilterLong;
    [SerializeField] private MeshCollider meshColShort;
    [SerializeField] private MeshCollider meshColLong;
    [SerializeField] private LayerMask layerMaskShort;
    [SerializeField] private LayerMask layerMaskLong;
    [SerializeField] private int edgeResolveIterations = 8;
    [SerializeField] private float edgeDistanceThreshold = 0.5f;
    [SerializeField] private float meshResolution = 2;
    [SerializeField] public float meshHeight = 0.5f;
    [SerializeField] Material meshMaterialFar;
    [SerializeField] Material meshMaterialClose;


    private void Start()
    {
        meshShort = new Mesh {name = "Short View Mesh"};
        meshLong = new Mesh {name = "Long View Mesh"};
        meshFilterShort.mesh = meshShort;
        meshFilterLong.mesh = meshLong;
    }

    public void MeshUpdate(EnemyCharacter enemy)
    {
        if (enemy == null)
        {
            if (meshFilterShort.mesh != meshShort) return;

            meshFilterShort.mesh = null;
            meshFilterLong.mesh = null;

            return;
        }

        if (meshFilterShort.mesh != meshShort)
        {
            meshFilterShort.mesh = meshShort;
            meshFilterLong.mesh = meshLong;
        }

        var enemyRotY = enemy.transform.rotation.eulerAngles.y;
        var enemyPos = enemy.transform.position;

        meshMaterialFar.SetFloat("_ViewDir", enemyRotY);
        meshMaterialClose.SetFloat("_ViewDir", enemyRotY);
        meshMaterialFar.SetVector("_EnemyPos", enemyPos);
        meshMaterialClose.SetVector("_EnemyPos", enemyPos);

        DrawViewMesh(enemy, layerMaskShort, meshShort, meshColShort);
        DrawViewMesh(enemy, layerMaskLong, meshLong, meshColLong);
    }

    private void DrawViewMesh(EnemyCharacter enemy, LayerMask layerMask, Mesh mesh, MeshCollider meshCol)
    {
        origin = enemy.transform.position - Vector3.up * meshHeight;

        int rayCount = Mathf.RoundToInt(enemy.fovAngle * enemy.fovAngleModifier * meshResolution);
        float angleIncrease = enemy.fovAngle * enemy.fovAngleModifier / rayCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        float range;
        switch (mesh.name)
        {
            case "Long View Mesh":
                range = enemy.viewDistance * enemy.viewDistanceModifier * 1.5f;
                break;
            default:
                range = enemy.viewDistance * enemy.viewDistanceModifier;
                break;
        }

        for (int i = 0; i <= rayCount; i++)
        {
            float angle = enemy.transform.eulerAngles.y - enemy.fovAngle * enemy.fovAngleModifier / 2 + angleIncrease * i;
            ViewCastInfo newViewCast = ViewCast(enemy, angle, layerMask, range);

            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDistanceThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(enemy, oldViewCast, newViewCast, layerMask, range);
                    if (edge.pointA != Vector3.zero)
                        viewPoints.Add(edge.pointA);
                    if (edge.pointB != Vector3.zero)
                        viewPoints.Add(edge.pointB);
                }
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = origin;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = viewPoints[i];
            if (i >= vertexCount - 2) continue;

            triangles[i * 3] = 0;
            triangles[i * 3 + 1] = i + 1;
            triangles[i * 3 + 2] = i + 2;
        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        //mesh.bounds = new Bounds(origin, Vector3.one * 1000f); //se tiver sumindo com o player distante da origem precisa atualizar aqui
    }

    private ViewCastInfo ViewCast(EnemyCharacter enemy, float globalAngle, LayerMask layermask, float distance)
    {
        Vector3 dir = GetVectorFromAngle(enemy, globalAngle, true);
        RaycastHit hit;
        if (Physics.Raycast(origin, dir, out hit, distance, layermask))
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);

        return new ViewCastInfo(false, origin + dir * distance, distance, globalAngle);
    }

    private EdgeInfo FindEdge(EnemyCharacter enemy, ViewCastInfo minViewCast, ViewCastInfo maxViewCast, LayerMask layerMask, float range)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(enemy, angle, layerMask, range);
            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDistanceThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
        {
            pointA = _pointA;
            pointB = _pointB;
        }
    }

    private Vector3 GetVectorFromAngle(EnemyCharacter enemy, float angle, bool angleIsGlobal)
    {
        if (!angleIsGlobal && enemy != null)
            angle += enemy.transform.eulerAngles.y;
        return new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
    }
}