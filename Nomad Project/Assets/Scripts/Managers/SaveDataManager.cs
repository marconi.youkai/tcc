﻿using System.IO;
using UnityEngine;

public class SaveDataManager : MonoBehaviour
{
    public static SaveDataManager tutProvSave;
    public static SaveDataManager GetSave() { return tutProvSave; }
    //public Vector3 savePosition;
    //public bool firstSpawn = true;
    //public bool spawnCrouched = false;
    //public bool hadNerfgun = false;
    //public bool hadCookie = false;

    private void Awake()
    {
        if (tutProvSave == null)
        {
            DontDestroyOnLoad(gameObject);
            tutProvSave = this;
        }
        else
            Destroy(gameObject);
        //savePosition = Vector3.zero;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
            WriteSaveDataWithJson();
    }
    private void OnDestroy()
    {
        if (tutProvSave == this)
            tutProvSave = null;
    }
    public void ForceDestroySaveData()
    {
        Destroy(gameObject);
    }

    public void WriteSaveDataWithJson()
    {
        //SaveData.Instance.firstSpawn = false;
        SaveData.Instance.playerIsCrouched = FindObjectOfType<PlayerCharacter>().isCrouching;
        SaveData.Instance.playerPosition = FindObjectOfType<PlayerCharacter>().transform.position;
        SaveData.Instance.hasNerfgun = InventoryManager.inventoryManager.SearchForItem(ItemType.NerfGun);
        SaveData.Instance.hasCookie = InventoryManager.inventoryManager.SearchForItem(ItemType.Cookie);
        SaveData.Instance.playerRotation = FindObjectOfType<PlayerCharacter>().transform.rotation;

        //if (!Application.isEditor)
        //{
        string jsonSaveData = JsonUtility.ToJson(SaveData.Instance, true);
        using (StreamWriter writer = new StreamWriter(Application.dataPath + "\\Resources\\" + "save1.txt"))
        {
            writer.Write(jsonSaveData);
            writer.Dispose();
        }
        //}
        //else
        //{
        //    string jsonSaveData = JsonUtility.ToJson(SaveData.Instance, true);
        //    using (StreamWriter writer = new StreamWriter("C:\\Users\\2milho\\Desktop\\save1.txt"))
        //    {
        //        writer.Write(jsonSaveData);
        //        writer.Dispose();
        //    }
        //}


        //PlayerPrefs.SetString("PlayerData", jsonSaveData);
        //PlayerPrefs.Save();        
    }

    public void ReadSaveDataWithJson()
    {
        SaveData.CreateInstance<SaveData>();
        //string jsonFromPrefs = PlayerPrefs.GetString("PlayerData");
        StreamReader reader;
        reader = new StreamReader(Application.dataPath + "\\Resources\\" + "save1.txt");
        string jsonFromTxt = reader.ReadToEnd();
        reader.Dispose();
        JsonUtility.FromJsonOverwrite(jsonFromTxt, SaveData.Instance);
        //print($"playerlist[0] = {SaveData.Instance.characterList[0].name}");

        //if (SaveData.Instance.playerPosition != null)
        //    savePosition = (Vector3)SaveData.Instance.playerPosition;
        //firstSpawn = SaveData.Instance.firstSpawn;
        //spawnCrouched = SaveData.Instance.playerIsCrouched;
        //hadNerfgun = SaveData.Instance.hasNerfgun;
        //hadCookie = SaveData.Instance.hasCookie;
        //SaveData.Instance.testInt++;
        //print($"savedata test int {SaveData.Instance.testInt}");
    }
}
