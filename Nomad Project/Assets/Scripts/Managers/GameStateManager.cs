﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    #region Singleton
    public static GameStateManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);

        playerCharacter = FindObjectOfType<PlayerCharacter>();
    }
    private void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }
    #endregion

    public enum GameState { Empty, InGame, Paused, Dialog, Cinematic }

    public event Action<GameState> OnGameStateChange;

    public PlayerCharacter playerCharacter { get; private set; }
    
    
}
