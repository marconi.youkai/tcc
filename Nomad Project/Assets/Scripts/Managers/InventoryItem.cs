﻿using UnityEngine;

public class InventoryItem : MonoBehaviour, IUsable, IMouseOverable
{
    public Item Item { get; set; }

    public virtual CursorManager.CursorType MouseOverCursorType()
    {
        return CursorManager.CursorType.MouseOver;
    }

    public virtual CursorManager.CMStates[] MouseOverStatesAllowed()
    {
        return new CursorManager.CMStates[1] { CursorManager.CMStates.InGame };
    }

    public virtual string MouseOverTooltip()
    {
        return Item.description;
    }

    public virtual void Use() { }
    public virtual void Use(Transform target) { }
    public virtual void Use(Transform user, Transform target) { }
}
