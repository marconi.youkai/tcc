using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(ViewConeRenderer))]
public partial class StealthDetectionManager : MonoBehaviour
{
    #region Singleton

    public static StealthDetectionManager stealthDManager;

    public static StealthDetectionManager GetStealthDManager()
    {
        return stealthDManager;
    }

    #endregion

    public static ViewConeRenderer _viewConeRenderer;
    public static EnemyCharacter selectedEnemySingleton;

    private EnemyCharacter clickedEnemy;
    [SerializeField] Material meshMaterial1;
    [SerializeField] Material meshMaterial2;
    private List<PlayerCharacter> playerCharsList;
    private List<EnemyCharacter>[] enemyCharsList;
    [SerializeField] [Range(1, 6)] private int spreadEnemyUpdates;
    private float enemyUpdateInterval = 0.2f;
    private int enemyCount = 0;
    private float updateTimer = 0f;
    private int updateToggler = 0;

    #region Unity Methods

    private void Awake()
    {
        if (stealthDManager == null)
            stealthDManager = this;
        else
            Destroy(this); //singleton

        enemyCharsList = new List<EnemyCharacter>[spreadEnemyUpdates];
        for (int i = 0; i < enemyCharsList.Length; i++)
        {
            enemyCharsList[i] = new List<EnemyCharacter>();
        }

        playerCharsList = new List<PlayerCharacter>();
        enemyUpdateInterval = Time.fixedDeltaTime / spreadEnemyUpdates;
        //print("current timestep is: " + Time.fixedDeltaTime + " / and current enemyupdateinterval is: " + enemyUpdateInterval);
    }

    private void OnDestroy()
    {
        if (stealthDManager == this)
            stealthDManager = null; //singleton
    }

    private void Start()
    {
        _viewConeRenderer = gameObject.GetComponent<ViewConeRenderer>();
    }

    private void Update()
    {
        updateTimer += Time.deltaTime;
        if (updateTimer >= enemyUpdateInterval)
        {
            updateTimer -= enemyUpdateInterval;
            EnemyGroupUpdate();
        }
    }

    private void LateUpdate()
    {
        _viewConeRenderer.MeshUpdate(selectedEnemySingleton);
    }

    #endregion

    #region Input & List Registering

    public void ClickEnemy(InputAction.CallbackContext context)
    {
        if (!context.performed)
            return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, 100, 10240)) return;

        clickedEnemy = hit.transform.GetComponent<EnemyCharacter>();
        if (clickedEnemy == null) return;

        if (clickedEnemy != selectedEnemySingleton)
            clickedEnemy.Select();
        else
            clickedEnemy.Deselect();
    }

    public void RegisterEnemy(EnemyCharacter enemy) //start dos inimigos, separa em n listas (spreadenemyupdates)
    {
        int i = ++enemyCount % spreadEnemyUpdates;
        enemyCharsList[i].Add(enemy);
    }

    public void RegisterPlayer(PlayerCharacter player)
    {
        playerCharsList.Add(player);
    }

    public void UnregisterPlayer(PlayerCharacter player)
    {
        playerCharsList.Remove(player);
    }

    #endregion

    #region Enemy Updates

    private void EnemyGroupUpdate()
    {
        if (enemyCharsList == null)
            return;
        updateToggler = ++updateToggler % spreadEnemyUpdates;
        //print(updateToggler + " || update toggler");
        foreach (EnemyCharacter e in enemyCharsList[updateToggler])
        {
            if (playerCharsList.Count > 0)
            {
                e.EnemyUpdate(playerCharsList);
            }
            // else
            // {
            //     e.EnemyUpdate((PlayerCharacter) null);
            // }
        }
    }
    #endregion
}