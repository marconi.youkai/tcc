using UnityEngine;

public class VisualSensor : MonoBehaviour, ISensor
{
    private float meshHeight = 0;
    private void Start()
    {
        meshHeight = StealthDetectionManager.GetStealthDManager().gameObject.GetComponent<ViewConeRenderer>().meshHeight;
    }
    public bool Detected(EnemyCharacter e, PlayerCharacter p)
    {
        if (p == null)
            return false;

        bool result = false;
        float playerDistance = Vector3.Distance(e.transform.position, p.transform.position);
        if (!(playerDistance <= e.viewDistance * e.viewDistanceModifier * 1.5f)) return result;
        Vector3 dirToPlayer = (p.transform.position - e.transform.position).normalized;

        if (!(Vector3.Angle(e.transform.forward, dirToPlayer) < e.fovAngle * e.fovAngleModifier / 2f)) return result;

        RaycastHit hit;

        if (Physics.Raycast(e.transform.position - Vector3.up * meshHeight, dirToPlayer, out hit, playerDistance, 1024)) return result;

        if (playerDistance <= e.viewDistance * e.viewDistanceModifier || !p.isCrouching)
            result = true;

        if (p.isCrouching && Physics.Raycast(e.transform.position - Vector3.up * meshHeight, dirToPlayer, out hit, playerDistance, 512)) //low walls
            result = false;

        return result;
    }
}