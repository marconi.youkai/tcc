﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepAreaSwitch : MonoBehaviour
{
    public string groupName;
    public string stateName;
    [Header("Se temporário, especificar o estado de exit.")]
    public bool temporary = false;
    public string returnStateName;
    [Header("Se temporário, anexar áreas grudadas")]
    public FootstepAreaSwitch[] sisterAreas;
    public List<GameObject> objectsInside;

    private void Start()
    {
        objectsInside = new List<GameObject>();
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerCharacter pTemp = InventoryManager.GetInventoryManager().activePlayer;
        if (other.gameObject == pTemp.gameObject || other.gameObject.layer == 13)
        {
            objectsInside.Add(other.gameObject);
            AkSoundEngine.SetSwitch(groupName, stateName, other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (temporary)
        {
            bool steppedOut = true;
            PlayerCharacter pTemp = InventoryManager.GetInventoryManager().activePlayer;
            if (other.gameObject == pTemp.gameObject || other.gameObject.layer == 13)
            {
                objectsInside.Remove(other.gameObject);
                foreach (FootstepAreaSwitch sister in sisterAreas)
                {
                    if (sister.objectsInside.Contains(other.gameObject))
                        steppedOut = false;
                }
                if(steppedOut)
                AkSoundEngine.SetSwitch(groupName, returnStateName, other.gameObject);
            }
        }
    }
}
