﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using TMPro;

public class CinematicIntro : MonoBehaviour
{
    [SerializeField] VideoPlayer vp;
    [SerializeField] TextMeshProUGUI warningText;
    [SerializeField] TextMeshProUGUI earlierText;
    [SerializeField] string nextSceneName;
    [SerializeField] bool playWarning = true;
    private bool textStillThere = false;

    private void Start()
    {
        if (playWarning)
            StartCoroutine(FadeInOutText(0.5f, warningText, 7, 30));
        StartCoroutine(PlayVideo());
    }

    public void SkipScene()
    {
        SceneManager.LoadScene(nextSceneName);
    }

    private IEnumerator PlayVideo()
    {
        yield return new WaitForSeconds(0.1f);
        while (textStillThere)
            yield return new WaitForFixedUpdate();
        vp.Play();
        yield return new WaitForSeconds(0.5f);
        while (vp.isPlaying)
            yield return new WaitForFixedUpdate();
        vp.Stop();
        if (playWarning)
            StartCoroutine(FadeInOutText(1f, earlierText, 3, 100));
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(LoadTutorial());
    }

    private IEnumerator LoadTutorial()
    {
        while (textStillThere)
            yield return new WaitForFixedUpdate();
        SceneManager.LoadScene(nextSceneName);
    }

    private IEnumerator FadeInOutText(float waitBefore, TextMeshProUGUI tmp, float duration, int steps)
    {
        textStillThere = true;
        yield return new WaitForSeconds(waitBefore);
        for (int i = 0; i < steps + 1; i++) //fade in
        {
            tmp.color = new Color(tmp.color.r, tmp.color.g, tmp.color.b, (1f / steps) * i);
            yield return new WaitForSeconds(0.01f);
        }

        yield return new WaitForSeconds(duration);

        for (int i = 0; i < steps + 1; i++) //fade out
        {
            tmp.color = new Color(tmp.color.r, tmp.color.g, tmp.color.b, 1f - (1f / steps * i));
            yield return new WaitForSeconds(0.01f);
        }
        textStillThere = false;
    }
}
