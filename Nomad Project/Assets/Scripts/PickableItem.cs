﻿using UnityEngine;
using UnityEngine.Events;

public class PickableItem : InventoryItem , IInteractable<ICharacter>
{
    [SerializeField] protected ItemType itemType;
    [SerializeField] protected string description;
    [SerializeField] protected int marketValue;
    [SerializeField] protected int level;
    [SerializeField] protected GameObject inventoryPrefab;
    [SerializeField] protected GameObject groundPrefab;
    [SerializeField] protected float noiseOnInteract;
    [SerializeField] protected UnityEvent OnInteractEvents;

    private void Start()
    {
        Item = new Item(itemType, description, marketValue, level, inventoryPrefab, groundPrefab, true, noiseOnInteract);
    }

    public override void Use()
    {
        Obtain();
    }

    public void Obtain()
    {
        GameObject go = Instantiate(inventoryPrefab);
        InventoryItem temp = go.GetComponent<InventoryItem>();
        temp.Item = Item;
        //audioEvent.Step();
        InventoryManager.GetInventoryManager().Inventory.InsertItem(temp);
        OnInteractEvents.Invoke();
        Destroy(gameObject);
    }

    public virtual bool Interact(ICharacter condition)
    {
        Obtain();
        return true;
    }

    public override CursorManager.CursorType MouseOverCursorType()
    {
        MouseOverRaycaster.Instance.StartContinuousRefresh();
        if (Vector3.Distance(InventoryManager.GetInventoryManager().activePlayer.transform.position, transform.position) <= 1.4f)
            return CursorManager.CursorType.Rightclick;
        else
            return CursorManager.CursorType.Unavailable;
    }
}