using UnityEngine;
using UnityEngine.Events;

public class ColliderTrigger : MonoBehaviour
{
    [SerializeField] private LayerMask collisionLayers;
    [SerializeField] private bool singleActivation = true;
    [SerializeField] private UnityEvent triggeredEvents;

#if UNITY_EDITOR
    private void Start()
    {
        Collider col = this.gameObject.GetComponent<Collider>();
        
        if (!col)
            Debug.LogError("No colliders found, this trigger will not work", this);
    }
#endif

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("triggered");
        if (((1 << other.gameObject.layer) & collisionLayers) == 0)
            return;
        triggeredEvents.Invoke();
        if (!singleActivation)
            return;
        DisableAfterTrigger();
    }
 
    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("triggered");
        if (((1 << other.gameObject.layer) & collisionLayers) == 0)
            return;
        triggeredEvents.Invoke();
        if (!singleActivation)
            return;
        DisableAfterTrigger();
    }

    private void DisableAfterTrigger()
    {
        this.gameObject.SetActive(false);
    }
}