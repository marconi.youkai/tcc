﻿using UnityEngine;

public class IGadget : MonoBehaviour, IUsable
{
    public void Use() { }
    public void Use(Transform target) { }
    public void Use(Transform origin, Transform target) { }
}