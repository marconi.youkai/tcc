﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class TutorialProvSave : MonoBehaviour
{
    public GameObject playerGO;
    public Checkpoint initialSave;
    public GameObject YouDead;
    public GameObject YouWin;
    public PickableItem originalNerfGun;
    public ItemSpawner cookieJar;


    public void SetPlayerPos(GameObject player)
    {
        SaveDataManager.GetSave().ReadSaveDataWithJson();
        //if (SaveDataManager.GetSave().firstSpawn)
        //{
        //    SaveDataManager.GetSave().firstSpawn = false;
        //    //SaveDataManager.GetSave().savePosition = initialSave.transform.position;
        //    SaveDataManager.GetSave().spawnCrouched = initialSave.spawnCrouched;
        //}

        //StartCoroutine(delay(player));

        playerGO = player;
        YouDead.SetActive(false);
        YouWin.SetActive(false);
        playerGO.GetComponent<NavMeshAgent>().Warp((Vector3)SaveData.Instance.playerPosition);
        playerGO.transform.rotation = SaveData.Instance.playerRotation;
        PlayerCharacter pl = playerGO.GetComponent<PlayerCharacter>();
        pl.CrouchForce(SaveData.Instance.playerIsCrouched);
        //parte provisória do inventario
        if (SaveData.Instance.hasNerfgun)
            originalNerfGun.Interact(pl);
        if (SaveData.Instance.hasCookie)
            cookieJar.Obtain();


        //playerGO = player;
        //YouDead.SetActive(false);
        //YouWin.SetActive(false);
        //playerGO.GetComponent<NavMeshAgent>().Warp(SaveDataManager.GetSave().savePosition);
        //PlayerCharacter pl = playerGO.GetComponent<PlayerCharacter>();
        //pl.CrouchForce(SaveDataManager.GetSave().spawnCrouched);
        ////parte provisória do inventario
        //if (SaveDataManager.GetSave().hadNerfgun)
        //    originalNerfGun.Interact(pl);
        //if (SaveDataManager.GetSave().hadCookie)
        //    cookieJar.Obtain();
    }

    private IEnumerator delay(GameObject player)
    {
        yield return new WaitForSeconds(2f);
        playerGO = player;
        YouDead.SetActive(false);
        YouWin.SetActive(false);
        if (SaveData.Instance.playerPosition != null)
            playerGO.GetComponent<NavMeshAgent>().Warp((Vector3)SaveData.Instance.playerPosition);
        PlayerCharacter pl = playerGO.GetComponent<PlayerCharacter>();
        pl.CrouchForce(SaveData.Instance.playerIsCrouched);
        //parte provisória do inventario
        if (SaveData.Instance.hasNerfgun)
            originalNerfGun.Interact(pl);
        if (SaveData.Instance.hasCookie)
            cookieJar.Obtain();
    }

    public void Die()
    {
        YouDead.SetActive(true);
    }
    public void Win()
    {
        YouWin.SetActive(true);
    }
}
