﻿using UnityEngine;
using UnityEngine.AI;
using DialogSystem;

public class StepDialog : MonoBehaviour
{
    public bool playOnlyOnce = false;
    private bool played = false;

    private void OnTriggerEnter(Collider other)
    {
        if ((!played || !playOnlyOnce) && other.TryGetComponent(out PlayerCharacter p) && !p.seenByEnemy)
        {
            played = true;
            DialogHolder dialog = GetComponent<DialogHolder>();
            dialog.AssignDialog();
            //other.GetComponent<NavMeshAgent>().SetDestination(this.transform.position);
        }
    }


}