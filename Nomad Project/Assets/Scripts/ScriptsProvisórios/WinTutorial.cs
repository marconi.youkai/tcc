﻿using UnityEngine;

public class WinTutorial : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 8)
        {
            other.GetComponent<PlayerCharacter>().Win();
        }
    }
}
