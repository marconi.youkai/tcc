﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApresentacaoController : MonoBehaviour
{
    struct Slide
    {
        public int index;
        public GameObject go;
        public Vector3 idealPos;
        public Quaternion idealRot;
        public Slide(int _index, GameObject _go, Vector3 _v3, Quaternion _rot)
        {
            index = _index;
            go = _go;
            idealPos = _v3;
            idealRot = _rot;
        }
    }

    [SerializeField] private List<GameObject> slides;
    [SerializeField] private Transform startPos;
    [SerializeField] private Transform endPos;
    [SerializeField] private GameObject moveFX;
    [SerializeField] private float transitionDuration = 0.5f;
    private Transform cameraTr;
    private AudioSource audioS;
    private Vector3 camStablePos;
    private List<Slide> slideList;
    private Slide currentSlide;
    private Slide nextSlide;
    private Slide previousSlide;
    private int currentIndex = -1;
    private bool transitioning = false;

    private void Start()
    {
        moveFX.SetActive(false);
        cameraTr = Camera.main.transform;
        camStablePos = cameraTr.position;
        audioS = GetComponent<AudioSource>();
        int i = 0;
        slideList = new List<Slide>();
        currentSlide = new Slide(-1, gameObject, Vector3.zero, Quaternion.identity);
        foreach (GameObject go in slides)
        {
            slideList.Add(new Slide(i, go, go.transform.position, go.transform.rotation));
            i++;
            go.SetActive(false);
        }
        //JumpToNextSlide();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
            JumpToNextSlide();
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            JumpToPreviousSlide();
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("MainMenu");
    }

    private void JumpToNextSlide()
    {
        if (transitioning)
            return;

        currentIndex = ++currentIndex % slideList.Count;

        StartCoroutine(Transition());
    }

    private void JumpToPreviousSlide()
    {
        if (transitioning)
            return;

        currentIndex -= 1;
        if (currentIndex < 0)
            currentIndex = slideList.Count - 1;

        StartCoroutine(Transition());
    }

    private IEnumerator Transition()
    {
        moveFX.SetActive(true);
        audioS.Play();
        transitioning = true;
        float timer = 0;

        nextSlide = slideList[currentIndex];
        nextSlide.go.SetActive(true);
        nextSlide.go.transform.position = startPos.position;
        nextSlide.go.transform.rotation = startPos.rotation;

        if (currentSlide.index != -1)
            StartCoroutine(ExitCurrentSlide());

        while (nextSlide.go.transform.position != nextSlide.idealPos)
        {
            nextSlide.go.transform.position = Vector3.Lerp(startPos.position, nextSlide.idealPos, timer / transitionDuration);
            nextSlide.go.transform.rotation = Quaternion.Slerp(startPos.rotation, nextSlide.idealRot, timer / transitionDuration);
            if (timer > transitionDuration / 10)
                cameraTr.position = camStablePos + new Vector3(Random.Range(-2, 2) * Mathf.Clamp((1f - (timer / transitionDuration)), 0, 1), Random.Range(-2, 2) * Mathf.Clamp((1f - (timer / transitionDuration)), 0, 1), 0);
            timer += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        cameraTr.position = camStablePos;
        currentSlide = nextSlide;
        moveFX.SetActive(false);
        transitioning = false;
    }

    private IEnumerator ExitCurrentSlide()
    {
        float timer = 0;

        while (timer < transitionDuration / 3)
        {
            currentSlide.go.transform.position = Vector3.Slerp(currentSlide.idealPos, endPos.position, timer / (transitionDuration / 3));
            currentSlide.go.transform.rotation = Quaternion.Slerp(currentSlide.idealRot, endPos.rotation, timer / (transitionDuration / 3));
            timer += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        currentSlide.go.SetActive(false);
    }
}
