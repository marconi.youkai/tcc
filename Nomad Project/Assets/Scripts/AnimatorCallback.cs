﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorCallback : MonoBehaviour
{
    [SerializeField] PlayerCharacter playerChar;
    private void Shoot()
    {
        playerChar.NerfGunCallback();
    }
    private void End()
    {
        playerChar.EndShootingSequence();
    }
    private void Draw()
    {
        playerChar.RevealNerfGun();
    }
}
